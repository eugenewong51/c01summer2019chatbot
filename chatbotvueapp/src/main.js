import Vue from 'vue'
import 'es6-promise/auto' // promise for IE
import Axios from 'axios'
import GAuth from 'vue-google-oauth2'
import Vuex from 'vuex'
import App from './App.vue'
import Chat from 'vue-beautiful-chat'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import VueScrollTo from 'vue-scrollto'


Vue.config.productionTip = false
Vue.prototype.$http = Axios
if(process.env.NODE_ENV == "development"){
  console.log("dev")
  Axios.defaults.baseURL = process.env.API_URL

} else {
Axios.defaults.baseURL = 'http://localhost:8099'
console.log("happned")
}

Vue.use(Chat)
Vue.use(Vuex)
Vue.use(Buefy)
Vue.use(VueScrollTo, {
    container: "body",
     duration: 500,
     easing: "ease",
     offset: 0,
     force: true,
     cancelable: true,
     onStart: false,
     onDone: false,
     onCancel: false,
     x: false,
     y: true
})

import { corpus, corpusModal, crawler} from './store/corpus-store.js';

const gauthOption = {
  clientId: '1006176182232-8jmor4umefj86vd7p4j52c4hbk98mdeb.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}

Vue.use(GAuth, gauthOption)


export const store = new Vuex.Store({
  modules: {
    corpus,
    corpusModal,
    crawler
  }
})

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
