import axios from 'axios';
import {store} from './main.js'

export default {
    getAllDiscoveryDocs(){
        store.commit('isDocLoading', true)
        axios.get('/dfi/discovery/document')
            .then(response => {
                
                store.commit('setAllDiscoveryDocs', response.data)
                store.commit('setQna', response.data)
                store.commit('isDocLoading', false)

            })
            .catch(error => {
                console.log("error retriving all DFI docs..")
                console.log(error)
                store.commit('isDocLoading', false)
                
            })
    },

    sendFile(file){
        store.commit('isUploading', true)
        var formData = new FormData();
        formData.append("file", file);
        axios.post("/dfi/discovery/document/upload", 
            formData, 
            {
                headers: {'Content-Type': 'multipart/form-data'}
            }
            // config

            )
            .then(response =>  {
            
            console.log(response);
            store.commit('isUploading', false)
            store.commit('uploadFinished', {message:'file uploaded successfully', type:'is-success'})

            }, function (error) {
                console.log("file upload fail")
                console.log(error);
                store.commit('isUploading', false)
                store.commit('uploadFinished', {message:'error uploading file',type:'is-danger'})

            });
    }
    ,
    async deleteDFIQuestion(documentId){
        await axios.delete('/dfi/discovery/document/'+documentId)
            .then(response => {
                console.log(response)
                console.log("doc delete good")
            })
            .catch(error => {
                console.log(error)
                console.log("failed doc delete")
        })
    },

    async addQuestionToCorpus(doc){
        await axios.post('/dfi/discovery/document', {
            question: [doc.question],
            answer: [doc.answer]
        })
        .then(response => {
            console.log(response)
                console.log("doc added good")
            })
            .catch(error => {
                console.log(error)
                console.log("failed doc addd")
        })
    },

    async editQuestionInCorpus(doc, docId){
        await axios.put('/dfi/discovery/document/' + docId, {
            question: [doc.question],
            answer: [doc.answer]
        })
        .then(response => {
            console.log(response)
                console.log("doc editing good")
            })
            .catch(error => {
                console.log(error)
                console.log("failed doument editing")
        })
    },
    crawlWebsite(url,depth, name){
    //     const CancelToken = axios.CancelToken;
    // const source = CancelToken.source();
        name = "luceneindex"
        store.commit("setIsCrawling", true)
        var call = "/resources"
        console.log(url)
        console.log(url, depth)
        axios.post("/resources",{
            origin: url,
            depth:depth,
            name:name,
            company: {
                id: 1 // defaulted at 1, eventually get from session
            }
            },
            {
                headers: {'Content-Type': 'application/json'}
            }
        )
        .then(response => {
            console.log(response)
            this.getAllResources()
        store.commit("setIsCrawling", false)

        })
        .catch(error => {
            console.log(error)
        store.commit("setIsCrawling", false)

        })
    },
    updateIndexWebsite(url,depth, name){
        //     const CancelToken = axios.CancelToken;
        // const source = CancelToken.source();
            name = "luceneindex"
            store.commit("setIsCrawling", true)
            var call = "/resources"
            console.log(url)
            console.log(url, depth)
            axios.put("/resources",{
                origin: url,
                depth:depth,
                name:name,
                company: {
                    id: 1 // defaulted at 1, eventually get from session
                }
                },
                {
                    headers: {'Content-Type': 'application/json'}
                }
            )
            .then(response => {
                console.log(response)
                this.getAllResources()
            store.commit("setIsCrawling", false)
    
            })
            .catch(error => {
                console.log(error)
            store.commit("setIsCrawling", false)
    
            })
        },
    getAllResources(){
        store.commit('setLoadingResources', true)
        axios.get("resources")
            .then(response => {
                console.log(response.data)
                var resources = response.data
                console.log()
                var resourceGroups = []
                for(var i=0;i <resources.length;i++) {
                    console.log(resources[i])
                    resourceGroups.push(resources[i].name);

                }

                console.log(resourceGroups);
                store.commit('setResourceGroups',resourceGroups)
                store.commit('setResourceData', response.data)
                store.commit('setLoadingResources', false)
            })
            .catch(error => {
                console.log(error)
                store.commit('setLoadingResources', false)
            })
    },

    async deleteResource(id){
            await axios.delete('/resources/'+id+"/deleteOne")
                .then(response => {
                    console.log(response)
                    console.log("doc delete good")
                })
                .catch(error => {
                    console.log(error)
                    console.log("failed doc delete")
            })

    }

}
