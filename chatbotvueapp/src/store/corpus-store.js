
const corpus = {
    state: {
        allDiscoveryDocs: undefined,
        results: [],
        qna: [],
        isDocLoading: false,
        isUploading: false,
        uploadFinished: {},
        question: "",
        answer: ""
    },
    mutations: {
        setAllDiscoveryDocs(state, queryResponse){
                state.allDiscoveryDocs = queryResponse
        },
        setResults(state, queryResponse){
            if(queryResponse)
                state.results = queryResponse.results
            else
                state.results = []
        },
        setQna(state, queryResponse){
            if(queryResponse){
                console.log(queryResponse)
                var result;
                for(var i = 0; i < queryResponse.results.length; i++){
                    var result = queryResponse.results[i]
                    if(result.propertyNames.includes('question')
                        && result.propertyNames.includes('answer')){
                            state.qna.push(result)
                        }
                }
            } else {
                state.qna = []
            }
           
        },
        isDocLoading(state, bool){
            state.isDocLoading = bool
        },
        isUploading(state, bool){
            state.isUploading = bool
        },
        uploadFinished(state, {message, type}){
            state.uploadFinished = {
                message, 
                type
            }
        },
        addQuestion(state, question){
            state.question = question
        },
        addAnswer(state, answer){
            state.answer = answer
        }

    },
    getters: {
        allDiscoveryDocs: state => state.allDiscoveryDocs,
        results: state => state.results,
        qna: state => state.qna,
        isDocLoading: state => state.isDocLoading,
        isUploading: state => state.isUploading,
        uploadFinished: state => state.uploadFinished,
        question: state => state.question,
        answer: state => state.answer
    }
}

const corpusModal = {
    state: {
        suggestions: [],
        isComponentModalActive: false
      },
      mutations: {
        openCorpus(state, value){
          state.isComponentModalActive = value;
        },
        setSuggestions(state, list){
            state.suggestions = list
        }
      },
      getters: {
          isComponentModalActive: state => state.isComponentModalActive,
          suggestions: state => state.suggestions
        }
}

const crawler = {
    state: {
        resourceData: [],
        loadingResources: false,
        isCrawling: false,
        resourceGroups: []
    }, 
    mutations: {
        setResourceData(state, list){
            state.resourceData = list
        },
        setLoadingResources(state, bool){
            state.loadingResources = bool
        },
        setIsCrawling(state, bool){
            state.isCrawling = bool
        },
        setResourceGroups(state , list){
            state.resourceGroups = list
        }
    },
    getters: {
        resourceData: state => state.resourceData,
        loadingResources: state => state.loadingResources,
        isCrawling: state => state.isCrawling,
        resourceGroups: state => state.resourceGroups
        }
}

export {corpus, corpusModal, crawler}

