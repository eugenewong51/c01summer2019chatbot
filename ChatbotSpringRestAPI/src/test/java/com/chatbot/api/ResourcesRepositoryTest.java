package com.chatbot.api;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.chatbot.api.models.Resources;
import com.chatbot.api.repository.ResourcesRepository;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class ResourcesRepositoryTest extends TestCase{
	
	@Autowired
	private ResourcesRepository resourcesRepository;
	
	@Test
	public void addResourceThenGet() {
		Resources resource = new Resources();
		
		resource.setName("testResource");
		
		resource = resourcesRepository.save(resource);
		
		Resources result = resourcesRepository.findById(resource.getID()).orElse(new Resources());
		
		assertEquals("testResource", result.getName());
	}

}
