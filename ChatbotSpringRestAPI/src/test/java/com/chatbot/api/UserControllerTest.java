package com.chatbot.api;

import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.chatbot.api.controllers.UserController;
import com.chatbot.api.models.BotUser;
import com.chatbot.api.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private UserService service;

	@Test
	void testAddingUser() throws Exception {
		BotUser user = new BotUser();
		user.setGivenName("test");
		user.setFamilyName("test");
		user.setID(1);
		
		when(service.addUser(user)).thenReturn(user);
		this.mockMvc.perform(post("/users")
				.content(objectMapper.writeValueAsString(user))
				.contentType(MediaType.APPLICATION_JSON)
			    .characterEncoding("utf-8"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.givenName").value("test"))
				.andExpect(jsonPath("$.familyName").value("test"));
	}
	
	@Test
	void testGettingUserById() throws Exception {
		BotUser user = new BotUser();
		user.setID(1);
		user.setGivenName("test");
		user.setFamilyName("test");

		when(service.getUserById(1)).thenReturn(user);
		this.mockMvc.perform(get("/users/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.familyName").value("test"))
				.andExpect(jsonPath("$.givenName").value("test"));
	}
	
	@Test
	void testGettingAllUsers() throws Exception {
		BotUser user = new BotUser();
		BotUser user2 = new BotUser();

		user.setID(1);
		user.setFamilyName("test");
		user.setGivenName("test");
		
		user2.setID(2);
		user2.setFamilyName("test2");
		user2.setGivenName("test2");

		List<BotUser> users= new ArrayList<>();
		users.add(user);
		users.add(user2);
		
		when(service.getAllUsers()).thenReturn(users);
		this.mockMvc.perform(get("/users"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].familyName").value("test"))
				.andExpect(jsonPath("$[0].givenName").value("test"))
				.andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].familyName").value("test2"))
				.andExpect(jsonPath("$[1].givenName").value("test2"));
	}
}
