package com.chatbot.api;

import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import com.chatbot.api.controllers.ResourceController;
import com.chatbot.api.models.Resources;
import com.chatbot.api.repository.CompanyRepository;
import com.chatbot.api.services.ResourcesService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ResourcesControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private ResourcesService service;

	// @Autowired
	// private CompanyRepository companyRepository;

	// @Test
	// void testAddingResource() throws Exception {
	// 	Resources resource = new Resources();
	// 	resource.setID(1);
	// 	resource.setCompany(null);
	// 	resource.setName("testing");

	// 	when(service.addResource(resource)).thenReturn(resource);
	// 	this.mockMvc.perform(post("/resources")
	// 			.content(objectMapper.writeValueAsString(resource))
	// 			.contentType(MediaType.APPLICATION_JSON)
	// 		    .characterEncoding("utf-8"))
	// 			.andExpect(status().isOk())
	// 			.andExpect(jsonPath("$.id").value(1))
	// 			.andExpect(jsonPath("$.location").value("testing"));
	// }
	
	@Test
	void testGettingResourceById() throws Exception {
		Resources resource = new Resources();
		resource.setID(1);
		resource.setName("testing");

		when(service.getResourcesById(1)).thenReturn(resource);
		this.mockMvc.perform(get("/resources/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.name").value("testing"));
	}
	
	@Test
	void testGettingResourceByCompany() throws Exception {
		Resources resource = new Resources();
		Resources resource2 = new Resources();

		resource.setID(1);
		resource.setName("testing");
		
		resource2.setID(2);
		resource2.setName("testing2");

		List<Resources> resources = new ArrayList<>();
		resources.add(resource);
		resources.add(resource2);
		
		when(service.getResourcesByCompany(1)).thenReturn(resources);
		this.mockMvc.perform(get("/resources/byCompany/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].name").value("testing"));
	}
	
	@Test
	void testGettingAllResources() throws Exception {
		Resources resource = new Resources();
		resource.setID(1);
		resource.setName("testing");
		
		Resources resource2 = new Resources();
		resource2.setID(2);
		resource2.setName("testing2");

		Resources resource3 = new Resources();
		resource3.setID(3);
		resource3.setName("testing3");
		
		List<Resources> resources = new ArrayList<Resources>();
		
		resources.add(resource);
		resources.add(resource2);
		resources.add(resource3);
	
		when(service.getAllResources()).thenReturn(resources);
		this.mockMvc.perform(get("/resources"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].name").value("testing"))
				.andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].name").value("testing2"))
				.andExpect(jsonPath("$[2].id").value(3))
				.andExpect(jsonPath("$[2].name").value("testing3"));
	}
}
