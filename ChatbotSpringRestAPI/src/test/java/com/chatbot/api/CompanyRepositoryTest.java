package com.chatbot.api;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.chatbot.api.models.Company;
import com.chatbot.api.repository.CompanyRepository;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class CompanyRepositoryTest extends TestCase{
	
	@Autowired
	private CompanyRepository companyRepo;
	
	@Test
	public void addCompanyThenGet() {
		Company company = new Company();
		
		company.setID(1);
		company.setCompanyName("TestCompany");
		
		companyRepo.save(company);
		
		Company result = companyRepo.findById(1).orElse(new Company());
		
		assertEquals(result.getCompanyName(), "TestCompany");
	}
}
