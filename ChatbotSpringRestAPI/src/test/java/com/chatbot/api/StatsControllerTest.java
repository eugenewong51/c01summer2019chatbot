package com.chatbot.api;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import com.chatbot.api.controllers.StatsController;
import com.chatbot.api.models.Stats;
import com.chatbot.api.services.StatsService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class StatsControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper om;
	
	@MockBean
	private StatsService service;
	
	@Test
	void testAddingStat() throws Exception{
		Stats stat = new Stats();
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		when(service.addStat(stat)).thenReturn(stat);
		this.mockMvc.perform(post("/stats")
				.content(om.writeValueAsString(stat))
				.contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf-8"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.messageCount").value(45))
				.andExpect(jsonPath("$.sessionId").value("test"))
				.andExpect(jsonPath("$.noResponseCount").value(15));
	}
	
	@Test
	void testGetAllStats() throws Exception{
		Stats stat = new Stats();
		Stats stat2 = new Stats();
		Stats stat3 = new Stats();
		
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		stat2.setId(2);
		stat2.setMessageCount(30);
		stat2.setSessionId("test2");
		stat2.setNoResponseCount(30);
		
		stat3.setId(3);
		stat3.setMessageCount(15);
		stat3.setSessionId("test3");
		stat3.setNoResponseCount(45);
		
		List<Stats> statsList = new ArrayList<>();
		statsList.add(stat);
		statsList.add(stat2);
		statsList.add(stat3);
		
		when(service.getAllStats()).thenReturn(statsList);
		this.mockMvc.perform(get("/stats"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].messageCount").value(45))
				.andExpect(jsonPath("$[0].sessionId").value("test"))
				.andExpect(jsonPath("$[0].noResponseCount").value(15))
				.andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].messageCount").value(30))
				.andExpect(jsonPath("$[1].sessionId").value("test2"))
				.andExpect(jsonPath("$[1].noResponseCount").value(30))
				.andExpect(jsonPath("$[2].id").value(3))
				.andExpect(jsonPath("$[2].messageCount").value(15))
				.andExpect(jsonPath("$[2].sessionId").value("test3"))
				.andExpect(jsonPath("$[2].noResponseCount").value(45));
	}
	
	@Test
	void testGetStatBySessionId() throws Exception {
		Stats stat = new Stats();
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		when(service.getStatsBySessionId("test")).thenReturn(stat);
		this.mockMvc.perform(get("/stats/sessionId/test"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.messageCount").value(45))
				.andExpect(jsonPath("$.noResponseCount").value(15));
	}
	
	@Test
	void testGetStatById() throws Exception {
		Stats stat = new Stats();
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		when(service.getStatsById(1)).thenReturn(stat);
		this.mockMvc.perform(get("/stats/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.messageCount").value(45))
				.andExpect(jsonPath("$.noResponseCount").value(15));
	}
	
	@Test
	void testGetStatByUserId() throws Exception {
		Stats stat = new Stats();
		Stats stat2 = new Stats();
		
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		stat2.setId(2);
		stat2.setMessageCount(30);
		stat2.setSessionId("test2");
		stat2.setNoResponseCount(30);
		
		List<Stats> stats = new ArrayList<>();
		stats.add(stat);
		stats.add(stat2);
		
		when(service.getStatsByUserId(1)).thenReturn(stats);
		this.mockMvc.perform(get("/stats/userId/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].messageCount").value(45))
				.andExpect(jsonPath("$[0].noResponseCount").value(15))
				.andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].messageCount").value(30))
				.andExpect(jsonPath("$[1].noResponseCount").value(30));
	}

	@Test
	void testUpdateMessageCount() throws Exception {
		Stats stat = new Stats();
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		Stats stat2 = new Stats();
		stat2.setId(1);
		stat2.setMessageCount(46);
		stat2.setSessionId("test");
		stat2.setNoResponseCount(15);
		
		when(service.updateMessageCount("test")).thenReturn(stat2);
		this.mockMvc.perform(put("/stats/updateMessageCount/test"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.messageCount").value(46))
				.andExpect(jsonPath("$.noResponseCount").value(15));
	}
	
	@Test
	void testUpdateNoResponseCount() throws Exception {
		Stats stat = new Stats();
		stat.setId(1);
		stat.setMessageCount(45);
		stat.setSessionId("test");
		stat.setNoResponseCount(15);
		
		Stats stat2 = new Stats();
		stat2.setId(1);
		stat2.setMessageCount(45);
		stat2.setSessionId("test");
		stat2.setNoResponseCount(16);
		
		when(service.updateNoResponseCount("test")).thenReturn(stat2);
		this.mockMvc.perform(put("/stats/updateNoResponse/test"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.messageCount").value(45))
				.andExpect(jsonPath("$.noResponseCount").value(16));
	}

}
