package com.chatbot.api;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.chatbot.api.models.Stats;
import com.chatbot.api.repository.StatsRepository;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class StatsRepositoryTest {

	@Autowired
	private StatsRepository repo;
	
	@Test
	void addingThenGettingById() {
		Stats stat = new Stats();
		stat.setId(1);
		stat.setMessageCount(15);
		stat.setNoResponseCount(60);
		stat.setSessionId("test");
		
		stat = repo.save(stat);
		
		Stats result = repo.findById(stat.getId()).orElse(new Stats());
		
		assertEquals(result.getMessageCount(), 15);
		assertEquals(result.getNoResponseCount(), 60);
		assertEquals(result.getSessionId(), "test");
	}

	@Test
	void addingThenGettingBySessionId() {
		Stats stat = new Stats();
		stat.setMessageCount(30);
		stat.setNoResponseCount(60);
		stat.setSessionId("test1");
		
		repo.save(stat);
		
		Stats result = repo.findBySessionId("test1");
		
		assertEquals(result.getMessageCount(), 30);
		assertEquals(result.getNoResponseCount(), 60);
		assertEquals(result.getSessionId(), "test1");
	}
	
	@Test
	void addingThenUpdatingMessageCount() {
		Stats stat = new Stats();
		stat.setMessageCount(45);
		stat.setNoResponseCount(60);
		stat.setSessionId("test2");
	
		repo.save(stat);
		repo.updateMessageCount("test2");
		
		Stats result = repo.findById(stat.getId()).orElse(new Stats());
		assertEquals(result.getMessageCount(), 46);
	}
	
	@Test
	void addingThenUpdatingNoResponseCount() {
		Stats stat = new Stats();
		stat.setMessageCount(15);
		stat.setNoResponseCount(60);
		stat.setSessionId("test3");

		repo.save(stat);
		repo.updateNoResponseCount("test3");
		
		Stats result = repo.findById(stat.getId()).orElse(new Stats());
		assertEquals(result.getNoResponseCount(), 61);
	}
}
