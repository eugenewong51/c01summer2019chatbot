package com.chatbot.api;

import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import com.chatbot.api.controllers.CompanyController;
import com.chatbot.api.models.Company;
import com.chatbot.api.services.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private CompanyService service;

	@Test
	void testAddingCompany() throws Exception {
		Company company = new Company();
		company.setCompanyName("Test");
		company.setID(1);

		when(service.addCompany(company)).thenReturn(company);
		this.mockMvc.perform(post("/company")
				.content(objectMapper.writeValueAsString(company))
				.contentType(MediaType.APPLICATION_JSON)
			    .characterEncoding("utf-8"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.companyName").value("Test"));
	}
	
	@Test
	void testGettingCompanyById() throws Exception {
		Company company = new Company();
		company.setCompanyName("Test");
		company.setID(2);

		when(service.getCompanyById(2)).thenReturn(company);
		this.mockMvc.perform(get("/company/2"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(2))
				.andExpect(jsonPath("$.companyName").value("Test"));
	}
	
	@Test
	void testGettingAllCompanies() throws Exception {
		Company company = new Company();
		company.setCompanyName("Test");
		company.setID(1);
		
		Company company2 = new Company();
		company2.setCompanyName("Test2");
		company2.setID(2);

		Company company3 = new Company();
		company3.setCompanyName("Test3");
		company3.setID(3);
		
		List<Company> companies = new ArrayList<Company>();
		
		companies.add(company);
		companies.add(company2);
		companies.add(company3);
	
		when(service.getAllCompanies()).thenReturn(companies);
		this.mockMvc.perform(get("/company"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].companyName").value("Test"))
				.andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].companyName").value("Test2"))
				.andExpect(jsonPath("$[2].id").value(3))
				.andExpect(jsonPath("$[2].companyName").value("Test3"));
	}
}
