package com.chatbot.api;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.chatbot.api.models.BotUser;
import com.chatbot.api.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
@AutoConfigureDataJpa
@ActiveProfiles("test")
class UserRepositoryTest {

	@Autowired
	private UserRepository repo;
	
	@Test
	void testAddingUserThenGet() {
		BotUser user = new BotUser();
		user.setCompanyID("test");
		user.setEmail("test");
		user.setFamilyName("test");
		user.setGivenName("test");
		
		user = repo.save(user);
		BotUser result = repo.findById(user.getID()).orElse(new BotUser());
		
		assertEquals(result.getCompanyID(), "test");
		assertEquals(result.getEmail(), "test");
		assertEquals(result.getFamilyName(), "test");
		assertEquals(result.getGivenName(), "test");
	}

	@Test
	void testGetUserByGoogleId() {
		BotUser user = new BotUser();
		user.setCompanyID("test2");
		user.setEmail("test2");
		user.setFamilyName("test2");
		user.setGivenName("test2");
		user.setGoogleId("test2");
		
		user = repo.save(user);
		BotUser result = repo.findByGoogleId("test2");
		
		assertEquals(result.getCompanyID(), "test2");
		assertEquals(result.getEmail(), "test2");
		assertEquals(result.getFamilyName(), "test2");
		assertEquals(result.getGivenName(), "test2");
	}
}
