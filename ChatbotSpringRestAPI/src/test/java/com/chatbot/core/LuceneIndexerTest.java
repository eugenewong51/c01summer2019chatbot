package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;

import com.chatbot.core.Fields;
import com.chatbot.core.HtmlPage;
import com.chatbot.core.HtmlStorage;
import com.chatbot.core.LuceneIndexer;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LuceneIndexerTest.class) 
public class LuceneIndexerTest extends TestCase {
  Path indexPath;
  Analyzer analyzer;
  LuceneIndexer indexer;

  HtmlStorage pages;
  
  Directory directory;
  DirectoryReader ireader;
  IndexSearcher isearcher;

  public LuceneIndexerTest() throws IOException, ParseException {
    indexPath = Files.createTempDirectory("tempIndex");
    analyzer = new StandardAnalyzer();
    indexer = new LuceneIndexer(analyzer);

    pages = new HtmlStorage();
    
    HtmlPage test1 = mock(HtmlPage.class);
    when(test1.getPath()).thenReturn("path one");
    when(test1.getTitle()).thenReturn("Test 1");
    when(test1.getTextData()).thenReturn("My first Heading My first paragraph.");
    
    HtmlPage test2 = mock(HtmlPage.class);
    when(test2.getPath()).thenReturn("path two");
    when(test2.getTitle()).thenReturn("Test 2");
    when(test2.getTextData()).thenReturn("The quick orange brown fox jumped over the lazy dog. To be or not to be that is the question.");
    
    HtmlPage test3 = mock(HtmlPage.class);
    when(test3.getPath()).thenReturn("path three");
    when(test3.getTitle()).thenReturn("Test 3");
    when(test3.getTextData()).thenReturn("Test 3 The quick quick brown fox jumped over the lazy dog. To be or not to be that is the question.");
    
    
    //System.out.println(test1.getTitle() + test1.getTitle());
    pages.add(test1);
    pages.add(test2);
    pages.add(test3);

    indexer.index(pages, indexPath);

    directory = FSDirectory.open(indexPath);
    ireader = DirectoryReader.open(directory);
    isearcher = new IndexSearcher(ireader);
 
  }

  @Test
  public void testOne() throws IOException, ParseException {
    QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
    Query query = parser.parse("orange"); // pass in a query
    TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
    ScoreDoc[] hits = topDocs.scoreDocs;

    for (int i = 0; i < hits.length; i++) {
      org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[i].doc);
      assertEquals("Test 2" , hitDoc.get(Fields.TITLE.toString()));
    }
  }

  @Test
  public void testTwo() throws IOException, ParseException {
    QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
    Query query = parser.parse("heading"); // pass in a query
    TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
    ScoreDoc[] hits = topDocs.scoreDocs;

    for (int i = 0; i < hits.length; i++) {
      org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[i].doc);
      assertEquals("Test 1", hitDoc.get(Fields.TITLE.toString()));
    }
  }

  @Test
  public void testThree() throws IOException, ParseException {
    QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
    Query query = parser.parse("quick"); // pass in a query
    TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
    ScoreDoc[] hits = topDocs.scoreDocs;
   

    org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[0].doc);
    assertEquals("Test 3", hitDoc.get(Fields.TITLE.toString()));

    org.apache.lucene.document.Document hitDoc1 = isearcher.doc(hits[1].doc);
    assertEquals("Test 2", hitDoc1.get(Fields.TITLE.toString()));

  }
  
  @Test
  public void testFour() throws IOException, ParseException {
    QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
    Query query = parser.parse("3"); // pass in a query
    TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
    ScoreDoc[] hits = topDocs.scoreDocs;
   

    org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[0].doc);
    //System.out.println(hitDoc.get("path"));
    assertEquals("Test 3", hitDoc.get(Fields.TITLE.toString()));

    //org.apache.lucene.document.Document hitDoc1 = isearcher.doc(hits[1].doc);
    //assertEquals("Test 2", hitDoc1.get("page title"));
  }
  
  @Test
  public void testAddDelete() throws IOException, ParseException {
    // test delete and add
    indexPath = Files.createTempDirectory("tempIndex2");

    HtmlPage page1 = mock(HtmlPage.class);
    when(page1.getPath()).thenReturn("test.com 123");
    when(page1.getTitle()).thenReturn("doc1");
    when(page1.getTextData()).thenReturn("content");
    
    HtmlPage page2 = mock(HtmlPage.class);
    when(page2.getPath()).thenReturn("test.com");
    when(page2.getTitle()).thenReturn("doc2");
    when(page2.getTextData()).thenReturn("content");
    
    HtmlPage page3 = mock(HtmlPage.class);
    when(page3.getPath()).thenReturn("test.com/123");
    when(page3.getTitle()).thenReturn("doc3");
    when(page3.getTextData()).thenReturn("content");
    
    HtmlPage page4 = mock(HtmlPage.class);
    when(page4.getPath()).thenReturn("test 123");
    when(page4.getTitle()).thenReturn("doc4");
    when(page4.getTextData()).thenReturn("content");
    
    HtmlPage page5 = mock(HtmlPage.class);
    when(page5.getPath()).thenReturn("test-123");
    when(page5.getTitle()).thenReturn("doc5");
    when(page5.getTextData()).thenReturn("content");

    
    indexer = new LuceneIndexer(analyzer);
    indexer.index(new HtmlStorage(), indexPath);
    directory = FSDirectory.open(indexPath);
    
    ireader = DirectoryReader.open(directory);
    isearcher = new IndexSearcher(ireader);
    testFive();
    
    indexer.addDocument(page1, indexPath);
    ireader = DirectoryReader.open(directory);
    isearcher = new IndexSearcher(ireader);
    testSix();
    
    indexer.addDocument(page2, indexPath);
    indexer.addDocument(page3, indexPath);
    indexer.addDocument(page4, indexPath);
    indexer.addDocument(page5, indexPath);
    ireader = DirectoryReader.open(directory);
    isearcher = new IndexSearcher(ireader);
    testSeven();
    
    indexer.deleteDocumentByUrl("test", indexPath);
    indexer.deleteDocumentByUrl("test.com/123456", indexPath);
    indexer.deleteDocumentByUrl("test.com 123 456", indexPath);
    indexer.deleteDocumentByUrl("www.test-123", indexPath);
    ireader = DirectoryReader.open(directory);
    isearcher = new IndexSearcher(ireader);
    testEight();
    
    indexer.deleteDocumentByUrl("test.com", indexPath);
    ireader = DirectoryReader.open(directory);
    isearcher = new IndexSearcher(ireader);
    testNine();
    
    directory.close();
    ireader.close();
  }
  
  public void testFive() throws IOException, ParseException {
	  QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
	  Query query = parser.parse("content"); // pass in a query
	  TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
	  ScoreDoc[] hits = topDocs.scoreDocs;

	  assertEquals(0, hits.length);

  }
  public void testSix() throws IOException, ParseException {
	  QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
	  Query query = parser.parse("content"); // pass in a query
	  TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
	  ScoreDoc[] hits = topDocs.scoreDocs;
	  
	  assertEquals(1, hits.length);
	  org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[0].doc);
	  assertEquals("doc1", hitDoc.get(Fields.TITLE.toString()));
  }
  public void testSeven() throws IOException, ParseException {
	  QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
	  Query query = parser.parse("content"); // pass in a query
	  TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
	  ScoreDoc[] hits = topDocs.scoreDocs;

	  assertEquals(5, hits.length);
	  org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[0].doc);
	  assertEquals("doc1", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[1].doc);
	  assertEquals("doc2", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[2].doc);
	  assertEquals("doc3", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[3].doc);
	  assertEquals("doc4", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[4].doc);
	  assertEquals("doc5", hitDoc.get(Fields.TITLE.toString()));
  }
  
  public void testEight() throws IOException, ParseException {
	  QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
	  Query query = parser.parse("content"); // pass in a query
	  TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
	  ScoreDoc[] hits = topDocs.scoreDocs;

	  assertEquals(5, hits.length);
	  org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[0].doc);
	  assertEquals("doc1", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[1].doc);
	  assertEquals("doc2", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[2].doc);
	  assertEquals("doc3", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[3].doc);
	  assertEquals("doc4", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[4].doc);
	  assertEquals("doc5", hitDoc.get(Fields.TITLE.toString()));
  }
  
  public void testNine() throws IOException, ParseException {
	  QueryParser parser = new QueryParser(Fields.CONTENT.toString(), analyzer); 
	  Query query = parser.parse("content"); // pass in a query
	  TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE);
	  ScoreDoc[] hits = topDocs.scoreDocs;

	  assertEquals(4, hits.length);
	  org.apache.lucene.document.Document hitDoc = isearcher.doc(hits[0].doc);
	  assertEquals("doc1", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[1].doc);
	  assertEquals("doc3", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[2].doc);
	  assertEquals("doc4", hitDoc.get(Fields.TITLE.toString()));
	  hitDoc = isearcher.doc(hits[3].doc);
	  assertEquals("doc5", hitDoc.get(Fields.TITLE.toString()));
  }

}
