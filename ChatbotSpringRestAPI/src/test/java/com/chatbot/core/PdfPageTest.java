package com.chatbot.core;

import java.io.IOException;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;

import com.chatbot.core.PdfPage;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OpenNlpAnalyzerTest.class) 
public class PdfPageTest extends TestCase {
  
  @Test
  public void testOne() throws IOException{
	  String url = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
	  String title = "testOne";
	  PdfPage result = PdfPage.getPdfByUrl(url, title);
	  //System.out.println(result.getTextData());
      assertEquals(url , result.getPath());
      assertEquals("Dummy PDF file" , result.getTextData().trim());
      assertEquals(title , result.getTitle());
  }
  
  @Test
  public void testTwo() throws IOException{
	  String url = "https://www.w3.org";
	  String title = "testTwo";
	  Boolean pass = false;
	  try {
		  PdfPage.getPdfByUrl(url, title);
	  }
	  catch(IOException e) {
		  pass = true;
	  }
	  assertTrue(pass);
  }
  
  @Test
  public void testThree() throws IOException{
	  String url = "http://test-3.com";
	  String title = "testThree";
	  String content = "Content 123";
	  PdfPage result = new PdfPage(url, title, content);;
      assertEquals(url , result.getPath());
      assertEquals(content , result.getTextData().trim());
      assertEquals(title , result.getTitle());
  }

}
