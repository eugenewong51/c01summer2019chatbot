package com.chatbot.core;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;
import com.chatbot.core.HtmlStorage;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HtmlStorageTest.class) 
public class HtmlStorageTest extends TestCase {
  
  @Test
  public void testOne(){
    HtmlStorage pages = new HtmlStorage();
    
    HtmlPage test1 = mock(HtmlPage.class);
    when(test1.getPath()).thenReturn("path one");
    when(test1.getTitle()).thenReturn("Test 1");
    when(test1.getTextData()).thenReturn("My first Heading My first paragraph.");
    
    HtmlPage test2 = mock(HtmlPage.class);
    when(test2.getPath()).thenReturn("path two");
    when(test2.getTitle()).thenReturn("Test 2");
    when(test2.getTextData()).thenReturn("The quick orange brown fox jumped over the lazy dog. To be or not to be that is the question.");
    
    HtmlPage test3 = mock(HtmlPage.class);
    when(test3.getPath()).thenReturn("path three");
    when(test3.getTitle()).thenReturn("Test 3");
    when(test3.getTextData()).thenReturn("Test 3 The quick quick brown fox jumped over the lazy dog. To be or not to be that is the question.");

    pages.add(test1);
    pages.add(test2);
    pages.add(test3);
    
    Iterator<Indexable> hsi = pages.iterator();
    HtmlPage page;
    page = (HtmlPage) hsi.next();
    assertEquals("Test 1", page.getTitle());
    page = (HtmlPage) hsi.next();
    assertEquals("Test 2", page.getTitle());
    page = (HtmlPage) hsi.next();
    assertEquals("Test 3", page.getTitle());
    assertTrue(!(hsi.hasNext()));
  }


}
