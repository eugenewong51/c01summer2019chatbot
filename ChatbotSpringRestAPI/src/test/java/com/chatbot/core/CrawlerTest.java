package com.chatbot.core;

import java.util.Iterator;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import com.chatbot.core.Crawler;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrawlerTest.class) 
public class CrawlerTest extends TestCase {

  @Test
  public void testOne() throws ConnectionFailedException {
	  String url = "https://www.w3.org";
	  Crawler c = new Crawler(url, 0);
	  int count = 0;
	  HtmlStorage hs = c.crawl();
	  for (Indexable page:hs) {
		  count++;
		  assertEquals("https://www.w3.org", page.getPath());
		  assertEquals("World Wide Web Consortium (W3C)", page.getTitle());
	  }
	  assertEquals(1, count);
	  assertEquals(0, c.getInvalidLinksCount());
  }
  
  @Test
  public void testTwo() throws ConnectionFailedException {
	  String url = "http://www.blankwebsite.com/";
	  Crawler c = new Crawler(url, 1);
	  HtmlStorage hs = c.crawl();
	  Iterator<Indexable> hsi = hs.iterator();
	  HtmlPage p1 = (HtmlPage) hsi.next();
	  assertEquals("http://www.blankwebsite.com/", p1.getPath());
	  assertEquals("Blank website. Blank site. Nothing to see here.", p1.getTitle());
	  HtmlPage p2 = (HtmlPage) hsi.next();
	  assertEquals("https://www.pointlesssites.com/", p2.getPath());
	  assertEquals("PointlessSites.com - Fun Things To Do When You're Bored", p2.getTitle());
	  assertTrue(!(hsi.hasNext()));
	  assertEquals(0, c.getInvalidLinksCount());
  }
  
  @Test
  public void testThree() throws ConnectionFailedException {
	  String url = "url123";
	  Crawler c = new Crawler(url, 1);
	  HtmlStorage hs = null;
	  try {
		   hs = c.crawl();
	  }
	  catch( ConnectionFailedException e) {
		  assertTrue(true);
	  }
  }
  
  @Test
  public void testFour() throws ConnectionFailedException {
	  String url = "https://www.utsc.utoronto.ca/~nick/cscC63/additional-note/";
	  Crawler c = new Crawler(url, 1);
	  HtmlStorage hs = null;
	  try {
		   hs = c.crawl();
	  }
	  catch( ConnectionFailedException e) {
		  assertTrue(true);
	  }
	  
  }


}
