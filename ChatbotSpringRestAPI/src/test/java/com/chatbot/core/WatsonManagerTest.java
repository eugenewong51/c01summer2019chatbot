package com.chatbot.core;

import java.util.List;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;

import com.chatbot.core.watson.WatsonManager;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.RuntimeIntent;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WatsonManagerTest.class) 
public class WatsonManagerTest extends TestCase {
  WatsonManager watson_manager = WatsonManager.get_Watson_Manager();
  
  String session_id_1 = "1";
  String intent = "";

  public WatsonManagerTest() {

  }
 
  @Test
  public void testWatsonManager() {
    
    MessageResponse response = watson_manager.send_message(session_id_1, "index");
    // If an intent was detected, print it to the console.
    List<RuntimeIntent> responseIntents = response.getOutput().getIntents();
    if(responseIntents.size() > 0) {
      intent = responseIntents.get(0).getIntent();
      assertEquals(intent, "update_index");
    } else {
      assertEquals(false, true);
    }
       
    response = watson_manager.send_message(session_id_1, "dashboard");
    // If an intent was detected, print it to the console.
    responseIntents = response.getOutput().getIntents();
    if(responseIntents.size() > 0) {
      intent = responseIntents.get(0).getIntent();
      assertEquals(intent, "open_dashboard");
    } else {
      assertEquals(false, true);
    }
    
    response = watson_manager.send_message(session_id_1, "update corpus");
    // If an intent was detected, print it to the console.
    responseIntents = response.getOutput().getIntents();
    if(responseIntents.size() > 0) {
      intent = responseIntents.get(0).getIntent();
      assertEquals(intent, "update_discovery");
    } else {
      assertEquals(false, true);
    }
    
    response = watson_manager.send_message(session_id_1, "fintech dfi?");
    // If an intent was detected, print it to the console.
    String action = response.getOutput().getActions().get(0).getName();
    assertEquals(action, "CALL_DISCOVERY_OR_LUCENE");
    
  }
}
