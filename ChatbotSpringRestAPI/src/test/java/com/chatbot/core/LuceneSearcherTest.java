package com.chatbot.core;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;

import com.chatbot.core.Fields;
import com.chatbot.core.HtmlPage;
import com.chatbot.core.LuceneSearcher;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LuceneSearcherTest.class) 
public class LuceneSearcherTest extends TestCase{
  Path indexPath;
  Analyzer analyzer;
  IndexWriterConfig config;
  Directory directory;
  IndexWriter iWriter;

  ArrayList<HtmlPage> pages = new ArrayList<HtmlPage>();

  LuceneSearcher searcher;

  ArrayList<Document> results;
  ArrayList<String> highlight;

  public LuceneSearcherTest() throws IOException {
    indexPath = Files.createTempDirectory("tempIndex");
    analyzer = new StandardAnalyzer();
    searcher = new LuceneSearcher(analyzer);
    config = new IndexWriterConfig(analyzer);
    directory = FSDirectory.open(indexPath);
    iWriter = new IndexWriter(directory, config);

    pages = new ArrayList<HtmlPage>();

    HtmlPage test1 = mock(HtmlPage.class);
    when(test1.getPath()).thenReturn("test1url");
    when(test1.getTitle()).thenReturn("Test 1");
    when(test1.getTextData()).thenReturn("Test 1 My first Heading My first paragraph.");
    
    HtmlPage test2 = mock(HtmlPage.class);
    when(test2.getPath()).thenReturn("test2url");
    when(test2.getTitle()).thenReturn("Test 2");
    when(test2.getTextData()).thenReturn("Test 2 The quick orange brown fox jumped over the lazy dog. To be or not to be that is the question.");
    
    HtmlPage test3 = mock(HtmlPage.class);
    when(test3.getPath()).thenReturn("test3url");
    when(test3.getTitle()).thenReturn("Test 3");
    when(test3.getTextData()).thenReturn("Test 3 The quick quick brown fox jumped over the lazy dog. To be or not to be that is the question.");
    
    pages.add(test1);
    pages.add(test2);
    pages.add(test3);

    for (HtmlPage htmlPage : pages) {

      Document doc = new Document();
      // for now we will have two field per document
      doc.add(new Field(Fields.PATH.toString(), htmlPage.getPath(), TextField.TYPE_STORED));
      doc.add(new Field(Fields.TITLE.toString(), htmlPage.getTitle(), TextField.TYPE_STORED));
      doc.add(new Field(Fields.CONTENT.toString(), htmlPage.getTextData(), TextField.TYPE_STORED));
      // System.out.println(htmlPage.getTextData());
      // write the doc on to the index
      iWriter.addDocument(doc);

    }
    // close writer and dir
    iWriter.close();
    directory.close();
  }

  @Test
  public void testLuceneSearcher() throws IOException, ParseException, InvalidTokenOffsetsException {

    // Test One
    results = searcher.search(indexPath, "Heading");
    highlight = searcher.resultHighlight(indexPath, "Heading", Fields.CONTENT);

    for(Document res : results) {
      assertEquals(res.get(Fields.TITLE.toString()), "Test 1");
      //System.out.println(res.get("page title"));
    }

    for(String res : highlight) {
      assertEquals("Test 1 My first *Heading* My first paragraph.", res);
    }

  }

  @Test
  public void testTwo() throws IOException, ParseException, InvalidTokenOffsetsException {
    // Test Two
    results = searcher.search(indexPath, "orange");
    highlight = searcher.resultHighlight(indexPath, "orange", Fields.CONTENT);

    for(Document res : results) {
      assertEquals(res.get(Fields.TITLE.toString()), "Test 2");
      //System.out.println(res.get("page title"));
    }

    for(String res : highlight) {
      assertEquals("Test 2 The quick *orange* brown fox jumped over the lazy dog. To be or not to be that is the question.", res);
    }
  }

  @Test
  public void testThree() throws IOException, ParseException, InvalidTokenOffsetsException {
    // Test Three
    results = searcher.search(indexPath, "quick");
    highlight = searcher.resultHighlight(indexPath, "quick", Fields.CONTENT);

    assertEquals(results.get(0).get(Fields.TITLE.toString()), "Test 3");
    assertEquals(results.get(1).get(Fields.TITLE.toString()), "Test 2");

    assertEquals("Test 3 The *quick* *quick* brown fox jumped over the lazy dog. To be or not to be that is the question.",highlight.get(0) );
    assertEquals("Test 2 The *quick* orange brown fox jumped over the lazy dog. To be or not to be that is the question.", highlight.get(1));

    //System.out.println(searcher.jsonOutput(indexPath, "To be or not to be that is the question", Fields.CONTENT));
    String expectedJsonString = "[{\"result\":{\"HighLight\":\"Test 2 *The* quick orange brown fox jumped over *the* lazy dog. *To* *be* *or* *not* *to* *be* *that* *is* *the* *question*.\",\"title\":\"Test 2\",\"url\":\"test2url\"}},"
    		+ "{\"result\":{\"HighLight\":\"Test 3 *The* quick quick brown fox jumped over *the* lazy dog. *To* *be* *or* *not* *to* *be* *that* *is* *the* *question*.\",\"title\":\"Test 3\",\"url\":\"test3url\"}}]";
    
    
    
    assertEquals(expectedJsonString, searcher.jsonOutput(indexPath, "To be or not to be that is the question", Fields.CONTENT));
    expectedJsonString = "[{\"result\":{\"HighLight\":\"Test 3 The *quick* *quick* brown fox jumped over the lazy dog. To be or not to be that is the question.\",\"title\":\"Test 3\",\"url\":\"test3url\"}},"
        + "{\"result\":{\"HighLight\":\"Test 2 The *quick* orange brown fox jumped over the lazy dog. To be or not to be that is the question.\",\"title\":\"Test 2\",\"url\":\"test2url\"}}]";
    assertEquals(expectedJsonString, searcher.jsonOutput(indexPath, "quick", Fields.CONTENT));
    expectedJsonString = "[{\"result\":{\"HighLight\":\"Test 2 The quick *orange* brown fox jumped over the lazy dog. To be or not to be that is the question.\",\"title\":\"Test 2\",\"url\":\"test2url\"}}]";
    assertEquals(expectedJsonString, searcher.jsonOutput(indexPath, "orange", Fields.CONTENT));

  }
  

  @Test
  public void testResultAmountOne() {
	  searcher.changeResultAmount(-2);
	  assertEquals(10, searcher.getResultAmount());
  }

  @Test
  public void testResultAmountTwo() throws IOException, ParseException, InvalidTokenOffsetsException {
	  searcher.changeResultAmount(2);
	  assertEquals(2, searcher.getResultAmount());
	  results = searcher.search(indexPath, "Test");
	  highlight = searcher.resultHighlight(indexPath, "Test", Fields.CONTENT);
	  assertEquals(2, highlight.size());
	  assertEquals(2, results.size());
	  assertEquals(results.get(0).get(Fields.TITLE.toString()), "Test 1");
	  assertEquals(results.get(1).get(Fields.TITLE.toString()), "Test 2");
	  searcher.changeResultAmount(10);
  }

}
