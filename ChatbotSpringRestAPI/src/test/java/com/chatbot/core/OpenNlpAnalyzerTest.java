package com.chatbot.core;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;

import com.chatbot.core.Fields;
import com.chatbot.core.OpenNlpAnalyzer;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OpenNlpAnalyzerTest.class) 
public class OpenNlpAnalyzerTest extends TestCase {

  @Test
  public void testcreateComponents() throws IOException {
    String text = "The quick brown fox jumped over the lazy dogs.";

    Analyzer analyzer = new OpenNlpAnalyzer();

    // Create token stream
    TokenStream tokenStream1 = analyzer.tokenStream(Fields.CONTENT.toString(), new StringReader(text));

    ArrayList<String> testtokens1 = new ArrayList<String>();

    testtokens1.add("quick:JJ");
    testtokens1.add("brown:JJ");
    testtokens1.add("fox:NN");
    testtokens1.add("jump:VBD");
    testtokens1.add("over:IN");
    testtokens1.add("lazy:JJ");
    testtokens1.add("dog:NNS");

    assertEquals(testtokens1.equals(getTokenArray(tokenStream1)), true);

    // Close token stream
    tokenStream1.close();

    // Create token stream
    text = "Your your you're the a is do does what A.S.A.P ASAP USA U.S.A";
    TokenStream tokenStream2 = analyzer.tokenStream(Fields.CONTENT.toString(), new StringReader(text));

    ArrayList<String> testtokens2 = new ArrayList<String>();

    testtokens2.add("your:PRP$");
    testtokens2.add("your:VBP");
    testtokens2.add("you:PRP");
    testtokens2.add("'re:VBP");
    testtokens2.add("do:VB");
    testtokens2.add("do:VBZ");
    testtokens2.add("what:WP");
    testtokens2.add("a.s.a.p:NNP");
    testtokens2.add("asap:NNP");
    testtokens2.add("usa:NNP");
    testtokens2.add("u.s.:NNP");

    assertEquals(testtokens2.equals(getTokenArray(tokenStream2)), true);

    // Create token stream
    tokenStream2.close();
    analyzer.close();
  }

  public ArrayList<String> getTokenArray(TokenStream tokenStream) throws IOException {

    // Used to get token word
    CharTermAttribute termAtt = tokenStream.addAttribute(CharTermAttribute.class);

    // Used to get token tag
    TypeAttribute typeAtt = tokenStream.addAttribute(TypeAttribute.class);

    ArrayList<String> tokens1 = new ArrayList<String>();
    try {
      tokenStream.reset();

      // Print all tokens until stream is exhausted

      while (tokenStream.incrementToken()) {
        // System.out.println(termAtt.toString() + ":" + typeAtt.type());
        tokens1.add(termAtt.toString() + ":" + typeAtt.type());
      }

      tokenStream.end();

    } finally {
      tokenStream.close();
    }

    return tokens1;

  }

}
