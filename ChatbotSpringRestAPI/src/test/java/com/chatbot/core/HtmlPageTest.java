package com.chatbot.core;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HtmlPageTest.class) 
public class HtmlPageTest extends TestCase {

  @Test
  public void testOne() throws IOException{
	  String url = "http://www.brainjar.com/java/host/test.html";
	  String title = "Test HTML File";
	  String text = "Test HTML File This is a very simple HTML file.";
	  
	  Document doc = mock(Document.class);
	  when(doc.baseUri()).thenReturn(url);
	  when(doc.title()).thenReturn(title);
	  when(doc.text()).thenReturn(text);
	  when(doc.clone()).thenReturn(doc);
	  Elements elements = mock(Elements.class);
	  when(elements.remove()).thenReturn(null);
	  when(doc.select("nav")).thenReturn(elements);
	  when(doc.select("header")).thenReturn(elements);
	  when(doc.select("footer")).thenReturn(elements);
	  
	  HtmlPage result = new HtmlPage(doc);
	  
	  // System.out.println(result.getTextData());
      assertEquals(url , result.getPath());
      assertEquals(text , result.getTextData().trim());
      assertEquals(title , result.getTitle());
  }
  
  @Test
  public void testTwo() throws IOException{
	  String url = "https://www.w3.org";
	  String title = "testTwo";
	  String content = "conten123";
	  HtmlPage result = new HtmlPage(url, title, content);
	  assertEquals(url , result.getPath());
	  assertEquals(content , result.getTextData().trim());
	  assertEquals(title , result.getTitle());
  }
  
  @Test
  public void testThree() {
	  Document doc = mock(Document.class);
	  when(doc.baseUri()).thenReturn("www.test1.com");
	  when(doc.title()).thenReturn("Test 1");
	  when(doc.text()).thenReturn("Test 1 My first Heading My first paragraph.");
	  when(doc.clone()).thenReturn(doc);
	  Elements elements = mock(Elements.class);
	  when(elements.remove()).thenReturn(null);
	  when(doc.select("nav")).thenReturn(elements);
	  when(doc.select("header")).thenReturn(elements);
	  when(doc.select("footer")).thenReturn(elements);
	  HtmlPage result = new HtmlPage(doc);
	  // System.out.println(result.getTextData());
	  assertEquals("www.test1.com" , result.getPath());
      assertEquals("Test 1 My first Heading My first paragraph." , result.getTextData().trim());
      assertEquals("Test 1" , result.getTitle());
  }
//  @Test
//  public void testFour() {
//	  String html = "<html>\r\n" + 
//	  		"  <head>\r\n" + 
//	  		"    <title>Title of the document</title>\r\n" + 
//	  		"    <style>\r\n" + 
//	  		"      li {\r\n" + 
//	  		"        display: inline-block; \r\n" + 
//	  		"        margin-right: 10px;\r\n" + 
//	  		"        color: #778899;\r\n" + 
//	  		"      }\r\n" + 
//	  		"    </style>\r\n" + 
//	  		"  </head>\r\n" + 
//	  		"  <body>\r\n" + 
//	  		"    <header>\r\n" + 
//	  		"      <nav>\r\n" + 
//	  		"        <ul style=\"padding:0;\">\r\n" + 
//	  		"          <li>Home</li>\r\n" + 
//	  		"          <li>About us</li>\r\n" + 
//	  		"        </ul>\r\n" + 
//	  		"      </nav>\r\n" + 
//	  		"      <h1>Welcome to our page</h1>\r\n" + 
//	  		"      <hr>\r\n" + 
//	  		"    </header>\r\n" + 
//	  		"    <article>\r\n" + 
//	  		"      <header>\r\n" + 
//	  		"        <h2>The section title</h2>\r\n" + 
//	  		"        <p>The text paragraph.</p>\r\n" + 
//	  		"      </header>\r\n" + 
//	  		"    </article>\r\n" +
//	  		"    <div class=\"main-content\">\r\n" + 
//	  		"      <h1>Main content</h1>\r\n" + 
//	  		"      <p>This is some paragraph. </p>\r\n" + 
//	  		"    </div>" + 
//	  		"  </body>\r\n" + 
//	  		"</html>";
//	  Document doc = Jsoup.parse(html);
//	  HtmlPage result = new HtmlPage(doc);
//	  System.out.println(result.getTextData());
//	  assertEquals("Title of the document Main content This is some paragraph.", result.getTextData());
//  }

}
