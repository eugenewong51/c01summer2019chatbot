package com.chatbot.core.watson.discovery;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonObject;
import com.ibm.cloud.sdk.core.http.HttpMediaType;
import com.ibm.cloud.sdk.core.service.security.IamOptions;
import com.ibm.watson.discovery.v1.Discovery;
import com.ibm.watson.discovery.v1.model.AddDocumentOptions;
import com.ibm.watson.discovery.v1.model.DeleteDocumentOptions;
import com.ibm.watson.discovery.v1.model.DeleteDocumentResponse;
import com.ibm.watson.discovery.v1.model.DocumentAccepted;
import com.ibm.watson.discovery.v1.model.QueryOptions;
import com.ibm.watson.discovery.v1.model.QueryResponse;
import com.ibm.watson.discovery.v1.model.QueryResult;

public class Connecter {
	
	private static Connecter discovery_instance= null;
	
	//school account cred, discovry not working
//	protected static String collectionId = "3b067e15-137c-4d65-b9c7-9c273926bc8d";
//	protected static String environmentId = "380d74a7-0153-45b1-b961-0b41846b0bc6";
//	private static String apiKey = "Kvjo5e18YHakVz-EsNn2T06qxSfJqrJg5K6paUx-Gg-z";
//	private static String url = "https://gateway-wdc.watsonplatform.net/discovery/api";
	
	//private account creds
	protected static String collectionId = "f6006f6a-e900-45dc-b749-d2cdfbe5f781";
	protected static String environmentId = "556afcb3-bcb8-458d-89bc-ac2649dc7b2a";
	private static String apiKey = "X3D7H8FCNjnx0T-RL5e_Wzem8PGzAV3tHuJLlVoam7cj";
	private static String url = "https://gateway.watsonplatform.net/discovery/api";
	
	protected static Discovery discovery;
	private static final String QUESTION = "question";
	private static final String ANSWER = "answer";
	
	private Connecter  () {
		
	}
	
	public static Connecter getInstance() {
		if(discovery_instance == null) {
			discovery_instance = new Connecter();
			IamOptions options=new IamOptions.Builder().apiKey(apiKey).build();
			discovery = new Discovery("2019-04-30", options);
			discovery.setEndPoint(url);	
		}
		
		return discovery_instance;
	}
	

	public QueryResponse askDiscovery(String query) {
		// build a query abd excecute it on the field question
		QueryOptions.Builder queryBuilder =new QueryOptions.Builder(environmentId, collectionId);
		
		
//		queryBuilder.naturalLanguageQuery(query);
//		QueryResponse queryResponse = discovery.query(queryBuilder.build()).execute().getResult();
//		queryBuilder.query(query);
//		queryBuilder.count(50);
//		QueryResponse queryResponse = discovery.query(queryBuilder.build()).execute().getResult();
		
		//match the query with the question field
		queryBuilder.query(QUESTION +":" + query);
		QueryResponse queryResponse = discovery.query(queryBuilder.build()).execute().getResult();
		// if there is no match with the question
		if(queryResponse.getResults().isEmpty()) {
			queryBuilder.query(ANSWER +":" + query);
			queryResponse = discovery.query(queryBuilder.build()).execute().getResult();
		}
		
		return queryResponse;
		
	}	
	
	public static void main(String[] args) {
		Connecter dh = Connecter.getInstance();
		System.out.println(dh.askDiscovery("").getResults().size());
//		List<QueryResult> res = dh.askDiscovery("").getResults();
//		Iterator<QueryResult> qr = res.iterator();
//		while(qr.hasNext()) {
//			System.out.println(qr.next().get("question"));
//		}
		
//		JsonObject jo = new JsonObject();
//		jo.addProperty("question", "A test question?");
//		jo.addProperty("answer", "a test answer");
//		
//		System.out.println("sending json...");
//		String str = jo.toString();
//		System.out.println(str);
//		InputStream is = new ByteArrayInputStream(str.getBytes());
//		AddDocumentOptions.Builder builder = new AddDocumentOptions.Builder(environmentId, collectionId);
//		builder.file(is);
//		builder.fileContentType(HttpMediaType.APPLICATION_JSON);
//		builder.filename("testjson");
//		DocumentAccepted response = discovery.addDocument(builder.build()).execute().getResult();
//		System.out.println(response);
		
		
//		DeleteDocumentOptions deleteRequest = new DeleteDocumentOptions.Builder(environmentId, collectionId, documentId).build();
//		DeleteDocumentResponse deleteResponse = discovery.deleteDocument(deleteRequest).execute().getResult();
	}

}
