package com.chatbot.core.watson;

import java.util.HashMap;
import java.util.logging.LogManager;

import com.ibm.cloud.sdk.core.service.security.IamOptions;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.CreateSessionOptions;
import com.ibm.watson.assistant.v2.model.DeleteSessionOptions;
import com.ibm.watson.assistant.v2.model.MessageInput;
import com.ibm.watson.assistant.v2.model.MessageOptions;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.SessionResponse;

public class WatsonManager {

  private static final WatsonManager watsonManager = new WatsonManager();
  private final String ASSISTANT_ID = "69594eaf-7897-46b8-98fc-928fe7dad8ae";
  private final String ASSISTANT_KEY = "3cw8D6TbYPur8AQZwD_xr763GN2LMxSic3M2IX25slIg";
  private Assistant assistant;
  private HashMap<String, String> watson_session_id = new HashMap<String, String>();

  private WatsonManager() {
    // Set up Assistant service.
    IamOptions iamOptions = new IamOptions.Builder().apiKey(ASSISTANT_KEY).build();
    assistant = new Assistant("2019-02-28", iamOptions);
  }

  public static WatsonManager get_Watson_Manager() {
    return WatsonManager.watsonManager;
  }

  public String create_session(String session_id) {
    assistant.setEndPoint("https://gateway-wdc.watsonplatform.net/assistant/api");

    // Create session.
    CreateSessionOptions createSessionOptions = new CreateSessionOptions.Builder(ASSISTANT_ID).build();
    SessionResponse session = assistant.createSession(createSessionOptions).execute().getResult();
    
    String watson_sid = session.getSessionId();
    watson_session_id.put(session_id, watson_sid);

    return session.getSessionId();
  }

  public void delete_session(String session_id) {
    // We're done, so we delete the session.
    DeleteSessionOptions deleteSessionOptions = new DeleteSessionOptions.Builder(ASSISTANT_ID, session_id).build();
    assistant.deleteSession(deleteSessionOptions).execute();
    
    watson_session_id.remove(session_id);
  }

  public MessageResponse send_message(String session_id, String message) {

    // Suppress log messages in stdout.
    LogManager.getLogManager().reset();
    
    // Create watson session if it does not already exist
    create_session(session_id);

    // Send message to assistant.
    MessageInput input = new MessageInput.Builder().text(message).build();
    MessageOptions messageOptions = new MessageOptions
        .Builder(ASSISTANT_ID, watson_session_id.get(session_id))
        .input(input)
        .build();
    MessageResponse response = assistant.message(messageOptions).execute().getResult();
   
//    Print Response
    
//    If an intent was detected, print it to the console.
//    List<RuntimeIntent> responseIntents = response.getOutput().getIntents();
//    if(responseIntents.size() > 0) {
//      System.out.println("Detected intent: #" + responseIntents.get(0).getIntent());
//    }
//
//    List<com.ibm.watson.assistant.v2.model.RuntimeEntity> responseEnitity = response.getOutput().getEntities();
//    if(responseEnitity.size() > 0) {
//      System.out.println("Detected intent: #" + responseEnitity.get(0).getEntity());
//    }
//
//    // Print the output from dialog, if any. Assumes a single text response.
//    List<DialogRuntimeResponseGeneric> responseGeneric = response.getOutput().getGeneric();
//    if(responseGeneric.size() > 0) {
//      for (DialogRuntimeResponseGeneric r : responseGeneric ){
//        System.out.println(r.getText());
//        System.out.println(r.getResponseType());
//        System.out.println(r.getOptions());
//      }
//    }
    
    return response;

  }


}
