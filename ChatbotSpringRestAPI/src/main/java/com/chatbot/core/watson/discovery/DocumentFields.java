package com.chatbot.core.watson.discovery;

public enum DocumentFields {
	QUESTION,
	ANSWER,
	SUBTITLE,
	HEADER,
	IMAGE,
	FOOTER,
	AUTHOR,
	TABLE,
	TABLE_OF_CONTENTS,
	TEXT,
	TITLE,
}
