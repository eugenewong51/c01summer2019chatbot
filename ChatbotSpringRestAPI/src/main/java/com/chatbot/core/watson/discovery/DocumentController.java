package com.chatbot.core.watson.discovery;

import com.ibm.watson.discovery.v1.model.QueryResponse;
import com.ibm.watson.discovery.v1.model.QueryResult;
import com.ibm.watson.discovery.v1.model.UpdateDocumentOptions;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ibm.cloud.sdk.core.http.HttpMediaType;
import com.ibm.watson.discovery.v1.Discovery;
import com.ibm.watson.discovery.v1.model.AddDocumentOptions;
import com.ibm.watson.discovery.v1.model.DeleteDocumentOptions;
import com.ibm.watson.discovery.v1.model.DeleteDocumentResponse;
import com.ibm.watson.discovery.v1.model.DocumentAccepted;
import com.ibm.watson.discovery.v1.model.DocumentStatus;
import com.ibm.watson.discovery.v1.model.GetDocumentStatusOptions;
import com.ibm.watson.discovery.v1.model.QueryOptions;
import com.ibm.watson.discovery.v1.model.QueryOptions.Builder;

public class DocumentController {
	private Discovery discovery;
	private final int MAX_DOCS = 9999; //IBM limit is 10000
	
	public DocumentController(){
		Connecter.getInstance();
		discovery = Connecter.discovery;
	}
	
	public QueryResponse getAllDocuments() {
		//get the builder and build an empty query
		Builder builder = new QueryOptions.
				Builder(Connecter.environmentId, Connecter.collectionId);
		builder.count(MAX_DOCS);
		// empty query to get all docs
		builder.query("");
		QueryResponse queryResponse = discovery
						.query(builder.build()).execute().getResult();
		
		return queryResponse;
	}
	
	public DocumentStatus getDocumentInfo(String documentId) {
		GetDocumentStatusOptions getOptions = 
				new GetDocumentStatusOptions.
					Builder(Connecter.environmentId, Connecter.collectionId, documentId).build();
		DocumentStatus response = discovery.getDocumentStatus(getOptions).execute().getResult();
		return response;
	}
	
	
	public QueryResponse getDocumentById(String documentId) {
		//get the builder and build an empty query
				Builder builder = new QueryOptions.
						Builder(Connecter.environmentId, Connecter.collectionId);
				builder.count(MAX_DOCS);
				// empty query to get all docs
				builder.query("id:"+ documentId);
		
				QueryResponse queryResponse = discovery
								.query(builder.build()).execute().getResult();
				
				return queryResponse;
	}
	
	public DocumentAccepted addDocument(String json) {
		// check if required fields present and add empty fields to supress discovery warnings
//		json = this.validateAndPreapreFields(json);
		
		InputStream is = new ByteArrayInputStream(json.getBytes());
		AddDocumentOptions.Builder builder = 
				new AddDocumentOptions.Builder(Connecter.environmentId, Connecter.collectionId);
		builder.file(is);
		builder.fileContentType(HttpMediaType.APPLICATION_JSON);
		Date date = new Date();
		builder.filename(date.toString().replace(" ", "")); // name to be changed later
		DocumentAccepted response = discovery.addDocument(builder.build()).execute().getResult();
		return response;
	}
	
	public DocumentAccepted addDocx(InputStream is) {
		// check if required fields present and add empty fields to supress discovery warnings
//		json = this.validateAndPreapreFields(json);
		
//		InputStream is = new ByteArrayInputStream(json.getBytes());
		AddDocumentOptions.Builder builder = 
				new AddDocumentOptions.Builder(Connecter.environmentId, Connecter.collectionId);
		builder.file(is);
		builder.fileContentType(HttpMediaType.APPLICATION_MS_WORD_DOCX);
		Date date = new Date();
		builder.filename(date.toString().replace(" ", "")); // name to be changed later
		DocumentAccepted response = discovery.addDocument(builder.build()).execute().getResult();
		return response;
	}
	
	public DocumentAccepted editDocument(String json, String documentId) {
		// check if required fields present and add empty fields to supress discovery warnings
//		json = this.validateAndPreapreFields(json);
		
		InputStream updatedDocumentStream = 
				new ByteArrayInputStream(json.getBytes());

		UpdateDocumentOptions.Builder updateBuilder = 
				new UpdateDocumentOptions.Builder(Connecter.environmentId, Connecter.collectionId, documentId);
		updateBuilder.file(updatedDocumentStream);
		
		Date date = new Date();
		updateBuilder.filename(date.toString().replace(" ", ""));
		updateBuilder.fileContentType(HttpMediaType.APPLICATION_JSON);
		
		DocumentAccepted updateResponse 
						= discovery.updateDocument(updateBuilder.build()).execute().getResult();
		return updateResponse;
	}
		
	public DeleteDocumentResponse deleteDocument(String documentId) {
		DeleteDocumentOptions deleteRequest = 
				new DeleteDocumentOptions.Builder(Connecter.environmentId, 
						Connecter.collectionId, documentId).build();
		
		DeleteDocumentResponse deleteResponse = discovery.deleteDocument(deleteRequest).execute().getResult();
		return deleteResponse;
	}
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		DocumentController dc = new DocumentController();
		System.out.println(dc.getAllDocuments());
		
		PrintWriter writer = new PrintWriter("the-file-name.json");
		writer.println(dc.getAllDocuments());
		writer.close();
		// example to add a file
//		File file = new File("C:\\Users\\daxpa\\Documents\\dfitestdocgibbrish.docx");
//        FileInputStream fis = new FileInputStream(file.getAbsolutePath());
//        
//        System.out.println(dc.addDocument(fis));
        
	}
}
