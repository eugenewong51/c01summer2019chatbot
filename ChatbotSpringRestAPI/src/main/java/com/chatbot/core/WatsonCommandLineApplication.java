package com.chatbot.core;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.util.logging.LogManager;

import com.chatbot.core.watson.Actions;
import com.chatbot.core.watson.discovery.Connecter;
import com.ibm.cloud.sdk.core.service.security.IamOptions;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.CreateSessionOptions;
import com.ibm.watson.assistant.v2.model.DeleteSessionOptions;
import com.ibm.watson.assistant.v2.model.DialogNodeAction;
import com.ibm.watson.assistant.v2.model.DialogRuntimeResponseGeneric;
import com.ibm.watson.assistant.v2.model.MessageInput;
import com.ibm.watson.assistant.v2.model.MessageOptions;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.RuntimeIntent;
import com.ibm.watson.assistant.v2.model.SessionResponse;
import com.ibm.watson.discovery.v1.model.QueryResult;

public class WatsonCommandLineApplication {

  public static void main(String[] args) throws FileNotFoundException {
    Scanner queryScanner = new Scanner(System.in);
    // Suppress log messages in stdout.
    LogManager.getLogManager().reset();

    // Set up Assistant service.
    IamOptions iamOptions = new IamOptions.Builder().apiKey("3cw8D6TbYPur8AQZwD_xr763GN2LMxSic3M2IX25slIg").build();
    Assistant service = new Assistant("2019-02-28", iamOptions);
    service.setEndPoint("https://gateway-wdc.watsonplatform.net/assistant/api");
    String assistantId = "69594eaf-7897-46b8-98fc-928fe7dad8ae"; // replace with assistant ID

    // Create session.
    CreateSessionOptions createSessionOptions = new CreateSessionOptions.Builder(assistantId).build();
    SessionResponse session = service.createSession(createSessionOptions).execute().getResult();
    String sessionId = session.getSessionId();
    
    // instantiate IBM discovery helper
    Connecter discovery = Connecter.getInstance();
    System.out.println(discovery.askDiscovery("Hello").getResults().toString());

    // Initialize with empty value to start the conversation.
    String inputText = "";
    String currentAction = "";

    // Main input/output loop
    do {
      // Send message to assistant.
      MessageInput input = new MessageInput.Builder().text(inputText).build();
      MessageOptions messageOptions = new MessageOptions.Builder(assistantId, sessionId)
          .input(input)
          .build();
      MessageResponse response = service.message(messageOptions).execute().getResult();
      
      //System.out.println(response.getOutput());
      
      //PrintWriter writer = new PrintWriter("watson_MessageResponse.json");
      //writer.println(response.getOutput());
      //writer.close(); 


      // Check for any actions requested by the assistant.
      List<DialogNodeAction> responseActions = response.getOutput().getActions();
      if(responseActions != null) {
        if(responseActions.get(0).getActionType().equals("client")) {
          currentAction = responseActions.get(0).getName();
          System.out.println(currentAction);
        }
      }
      
      if(currentAction.equals(Actions.CALL_DISCOVERY_OR_LUCENE.toString())) {
        List<QueryResult> res;
        res = discovery.askDiscovery(inputText).getResults();
        response.getOutput().getUserDefined().put("discovery", res);
        System.out.println(response.getOutput());
        PrintWriter writer = new PrintWriter("watson_MessageResponse.json");
        writer.println(response.getOutput());
        writer.close(); 
      }
      
      
      // If an intent was detected, print it to the console.
      List<RuntimeIntent> responseIntents = response.getOutput().getIntents();
      if(responseIntents.size() > 0) {
        System.out.println("Detected intent: #" + responseIntents.get(0).getIntent());
      }

      List<com.ibm.watson.assistant.v2.model.RuntimeEntity> responseEnitity = response.getOutput().getEntities();
      if(responseEnitity.size() > 0) {
        System.out.println("Detected intent: #" + responseEnitity.get(0).getEntity());
      }
      
      // Print the output from dialog, if any. Assumes a single text response.
      List<DialogRuntimeResponseGeneric> responseGeneric = response.getOutput().getGeneric();
      if(responseGeneric.size() > 0) {
        for (DialogRuntimeResponseGeneric r : responseGeneric ){
          System.out.println(r.getText());
          System.out.println(r.getResponseType());
          System.out.println(r.getOptions());
        }
      }
      

      // Prompt for next round of input.
      System.out.print(">> ");
      inputText = queryScanner.nextLine();
    } while(!inputText.equals("quit"));

    // We're done, so we delete the session.
    DeleteSessionOptions deleteSessionOptions = new DeleteSessionOptions.Builder(assistantId, sessionId).build();
    service.deleteSession(deleteSessionOptions).execute();
    
    queryScanner.close();
  }

}
