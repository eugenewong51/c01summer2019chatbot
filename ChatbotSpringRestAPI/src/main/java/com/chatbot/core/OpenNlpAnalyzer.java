package com.chatbot.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.opennlp.OpenNLPPOSFilter;
import org.apache.lucene.analysis.opennlp.OpenNLPTokenizer;
import org.apache.lucene.analysis.opennlp.tools.NLPPOSTaggerOp;
import org.apache.lucene.analysis.opennlp.tools.NLPSentenceDetectorOp;
import org.apache.lucene.analysis.opennlp.tools.NLPTokenizerOp;
import org.apache.lucene.analysis.opennlp.tools.OpenNLPOpsFactory;
import org.apache.lucene.analysis.util.ClasspathResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.util.AttributeFactory;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerModel;

public class OpenNlpAnalyzer extends Analyzer {
  // Stop words used (standard English stop words + punctuation)
  private static final CharArraySet STOP_WORDS = new CharArraySet(
      Arrays.asList("a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in",
          "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then",
          "there", "these", "they", "this", "to", "was", "will", "with", "."),
      true);

  @Override
  protected TokenStreamComponents createComponents(String fieldName) {
    try {
      // Resource loader used to build model for tokenizer from binary
      ResourceLoader resourceLoader = new ClasspathResourceLoader(
          ClassLoader.getSystemClassLoader());

      // Build model using binary and resource loader
      TokenizerModel tokenModel = OpenNLPOpsFactory.getTokenizerModel("en-token.bin",
          resourceLoader);

      // NLP Tokenizer built using model
      NLPTokenizerOp tokenizerOp = new NLPTokenizerOp(tokenModel);

      // Build sentence Model using resource loader and binary
      SentenceModel sentenceModel = OpenNLPOpsFactory.getSentenceModel("en-sent.bin",
          resourceLoader);

      // Build sentence detecting tokenizer using sentence model
      NLPSentenceDetectorOp sentenceDetectorOp = new NLPSentenceDetectorOp(sentenceModel);

      // Build tokenizer using both sentence detector and NLP tokenizer
      Tokenizer tokenizer = new OpenNLPTokenizer(AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY,
          sentenceDetectorOp, tokenizerOp);

      // Build model for POS tagging using resource loader and binary
      POSModel posModel = OpenNLPOpsFactory.getPOSTaggerModel("en-pos-maxent.bin", resourceLoader);

      // Build token filter for POS tagging
      NLPPOSTaggerOp posTaggerOp = new NLPPOSTaggerOp(posModel);
      TokenFilter posFilter = new OpenNLPPOSFilter(tokenizer, posTaggerOp);

      // Build lowercase filter
      LowerCaseFilter tokenFilter = new LowerCaseFilter(posFilter);

      // Build Stop Filter from STOP_WORDS
      StopFilter stopFilter = new StopFilter(tokenFilter, STOP_WORDS);

      // Create dictionary used for lemmatization
      InputStream is = getClass().getClassLoader().getResourceAsStream("en-lemmatizer.dict.txt");
      DictionaryLemmatizer lemmatizer = new DictionaryLemmatizer(is);
      is.close();

      // Build lemmatization filter
      LemmatizationFilter lemmaFilter = new LemmatizationFilter(stopFilter, lemmatizer);

      // POS as synonym
      // TypeAsSynonymFilter typeAsSynonymFilter = new
      // TypeAsSynonymFilter(stopFilter);
      // TokenFilter tokenFilter = new StopFilter(tokenFilter, null);

      return new TokenStreamComponents(tokenizer, lemmaFilter);
    } catch (IOException e) {
      System.out.println("Error building tokenizer or token filters.");
      e.printStackTrace();
      return null;
    }

  }
}
