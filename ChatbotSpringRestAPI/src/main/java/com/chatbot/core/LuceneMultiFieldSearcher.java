package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/*
 * class to represent the searcher
 */
public class LuceneMultiFieldSearcher {
	 private DirectoryReader ireader;
	 private Directory directory;
     private IndexSearcher isearcher;
     
     // assign an analyzer to each field
     private  PerFieldAnalyzerWrapper analyzerWrapper;
     private Map<String,Analyzer> fieldAnalyzerMap; 

     public LuceneMultiFieldSearcher(Analyzer defaultAnalyzer) {
    	 fieldAnalyzerMap = new HashMap<String,Analyzer>();
    	 analyzerWrapper = new PerFieldAnalyzerWrapper(defaultAnalyzer); // use user's analyzer as the default analyzer
     }
     
     public void addAnalyzerToField(String field, Analyzer analyzer) {
    	 if (fieldAnalyzerMap.containsKey(field)){
    		 fieldAnalyzerMap.replace(field, analyzer); // update analyzer of an existing field
    	 }
    	 else {
    		 fieldAnalyzerMap.put(field, analyzer); // map analyzer to a new field
    	 }
    	 
     }
     public Analyzer removeAnalyzer(String field) {
    	 return fieldAnalyzerMap.remove(field);
     }
     // given the index, search through the document's given fields and return a list of best results
     public ArrayList<Document> searchByField(Path indexPath, String userQuery, String[] fields) throws IOException, ParseException {
    	 this.directory = FSDirectory.open(indexPath); // location of the index
    	 this.ireader = DirectoryReader.open(this.directory); // read the dir
    	 this.isearcher = new IndexSearcher(ireader); // read the dir to search from it
    	 
    	 MultiFieldQueryParser parser = new MultiFieldQueryParser(fields, analyzerWrapper);// search through fields that is in String[]fields
    	 Query query = parser.parse(userQuery); // pass in a query
    	 TopDocs topDocs = isearcher.search(query, 10, Sort.RELEVANCE); // tops docs is the result set
    	 ScoreDoc[] hits = topDocs.scoreDocs;

    	 ArrayList<Document> hitDoc = new  ArrayList<Document>(); 
    	 for (ScoreDoc hit : hits) {
    		 hitDoc.add(isearcher.doc(hit.doc)); // get one doc
    	 }
    	 ireader.close();
    	 directory.close();

    	 return hitDoc;
     }
     
	
	 //some code to test the searcher class
//	 public static void main( String[] args ) throws Exception
//	    {
//	        System.out.println( "Hello World!" );
//	        Map<String,Analyzer> f1 = new HashMap<String,Analyzer>();
//	        PerFieldAnalyzerWrapper fw = new PerFieldAnalyzerWrapper(new StandardAnalyzer(),f1);
//	        f1.put("firstname", new KeywordAnalyzer());
//	        System.out.println( fw.toString() );
//	    }
	
}

