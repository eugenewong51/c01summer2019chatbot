package com.chatbot.core;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.util.IOUtils;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	Scanner scanner = new Scanner(System.in);  // Create a Scanner to read user input
    	
    	// as for a url and depth
    	System.out.print("Hi please enter a website you would like to index: ");
    	String url = scanner.nextLine();
    	
    	System.out.print("please enter desired depth (depth 0 is the first page): ");
    	int depth = scanner.nextInt();
    	
    	// create a crawler with url and depth
	    Crawler crawler = new Crawler(url, depth);
	    System.out.println("Crawling " + url + ", " + depth + " pages deep...");
        // call the cralw method that return an iterator on html pages object
	    HtmlStorage htmlStorage = crawler.crawl();
        Iterator<Indexable> pageIterator = htmlStorage.iterator();

        //check if anything crawled
        if(!pageIterator.hasNext()) {
        	System.out.println("Crawler failed.");
        	System.out.println("No pages to index : (");
        	System.out.println("Goodbye!");
        	scanner.close();
        	return;
        }
        
        // crawling successful
        System.out.println();
        System.out.println("crawling successful.");
        System.out.println(crawler.getInvalidLinksCount() + " invalid links found and were not crawled.");
        
        
        // create a temp dir with java to store indexes
        // later this will a stand alone part of the application
        // where we may chose a location on the server to save specific
        // indexes
        System.out.println("Creating temporary directory to store indexes.");
        Path indexPath = Files.createTempDirectory("tempIndex");
        
        Analyzer standardAnalyzer = new StandardAnalyzer();
        
        // initialize lucene indexer class
        // searcher and indexer both use standard analyzer for now
        // later we may customize the analyzer to meet our requirements
        LuceneIndexer indexer = new LuceneIndexer(standardAnalyzer);
        System.out.println("Making indexes for the crawled pages.");
        indexer.index(htmlStorage, indexPath);
        System.out.println("Index built successfully!");
        
//        System.out.println("these urls were indexed: ");
//        pageIterator = htmlStorage.getIterator();
//        while(pageIterator.hasNext()) {
//        	System.out.println(pageIterator.next().getUrl());
//        }
        
        // initialize searcher class
        LuceneSearcher searcher = new LuceneSearcher(standardAnalyzer);
        System.out.println();
        System.out.println();
        System.out.println("Search " + url + " till depth, " + depth);
        Scanner queryScanner = new Scanner(System.in);
        String userQuery = "";
        // based on user input keep searching
        while(!userQuery.equals("q") || userQuery.equals("")) {
        	//queryScanner = new Scanner(System.in);
        	 System.out.print("Please enter your search query (q to quit): ");
             userQuery = queryScanner.nextLine();
             
             if(!userQuery.isEmpty() && !userQuery.equals("q")) {
            	 // user the search method of the searcher in lucene searcher class
            	 // using the same directory path to where the index is stored
            	 ArrayList<Document> docs = searcher.search(indexPath, userQuery);
            	 if(docs.size() > 0) {
            		 System.out.println("These are your top results: ");
                     System.out.println();
                     printUrls(docs);
            	 } else {
            		 System.out.println("Sorry no relevant results.");
            	 }
                
                 System.out.println();

                 
             }
             queryScanner.reset();
        }
        // close all scanners
        scanner.close();
        queryScanner.close();
        // remove temp dir
        //IOUtils.rm(indexPath);
        System.out.println("Goodbye!");  
                
    }
    
    /*
     * prints the urls based on the url list
     */
    public static void printUrls(ArrayList<Document> docs) {
    	int num = 0;
    	for(Document doc: docs) {
    		num++;
    		System.out.println(num + ") " + doc.get(Fields.PATH.toString()));
    	}
    }
}
