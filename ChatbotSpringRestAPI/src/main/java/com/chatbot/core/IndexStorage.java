package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Path;

public interface IndexStorage {
	
	/*
	 * given a path to a lucene index directory on system 
	 * store it in the storage under the name indexName
	 * if a storage with the same name exits then throw
	 * IndexExistsException
	*/
	public void store(String indexName, Path path) throws IndexExistsException;
	
	/*
	 * given an indexName retrieve all index files for that index
	 * to a location on the system and return the path to the
	 * index directory, throws IOException if there is an
	 * error creating local index
	 */
	public Path retrive(String indexname) throws IOException;
	
	
	/*
	 * given an indexname, remove the index files from
	 * the storage
	 */
	public void delete(String indexName) throws NoSuchIndexException;
	
	
	/*
	 * given an a path to lucene index, use that index to
	 * replace index located at indexName, if there is no
	 * index under that name then throw NoSuchIndexException
	 */
	public void update(String indexName, Path path) throws NoSuchIndexException;
	
}
