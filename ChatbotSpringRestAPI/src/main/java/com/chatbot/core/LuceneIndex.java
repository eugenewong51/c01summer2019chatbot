package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Path;

import com.chatbot.api.beans.SpringContext;
import com.chatbot.api.models.Resources;
import com.chatbot.api.repository.ResourcesRepository;
import com.chatbot.api.services.ResourcesService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// thread safe lucene index
public class LuceneIndex  
{ 
  // private instance, so that it can be 
  // accessed by only by getInstance() method 
    private static LuceneIndex instance;
    private static String bucketName;
    private static Path indexPath;
    private static IndexStorage lism;
    private static Logger logger = LoggerFactory.getLogger(LuceneIndex.class);

    private LuceneIndex() throws IOException {
        // private constructor
        // get the default index bucket name
        // assume there is a default index in the system
        
        LuceneIndex.refreshLocalIndex();
    }

        // synchronized method to control simultaneous access
    synchronized public static LuceneIndex getInstance() throws IOException 
    { 
        if (instance == null)                  
        { 
        // if instance is null, initialize 
        instance = new LuceneIndex(); 
        } 
        return instance; 
    }

    private static ResourcesService getResourcesService() {
        return SpringContext.getBean(ResourcesService.class);
    }
    
    public static Path getIndexPath() {
        return indexPath;
    }
    
    private static void setLism(IndexStorage lism) {
        LuceneIndex.lism = lism;
    }

    public static String getBucketName() {
        return bucketName;
    }

    private static void setBucketName(String bucketName) {
        LuceneIndex.bucketName = bucketName;
    }

    


    public static void refreshLocalIndex() throws IOException {
        LuceneIndex.logger.info("Getting the index from index storage");
        ResourcesService rs = getResourcesService();
 
        LuceneIndex.setBucketName(rs.getDefaultResource());
        LuceneIndex.setLism(new LuceneIndexStorageManager());
        indexPath = LuceneIndex.lism.retrive(bucketName);
        LuceneIndex.logger.info("Retrived " + bucketName + " index");

    }

} 
