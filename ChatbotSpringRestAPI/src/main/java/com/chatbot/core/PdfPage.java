package com.chatbot.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class PdfPage implements Indexable {
	private String url;
	private String textData; // this is a single string containing
	private String title;
	// a class that represents a pdf file will expand later
	
	public static PdfPage getPdfByUrl(String url, String title) throws IOException {
		System.out.println("opening "+url);
		// temporarily change standard out so that debug messages wont fill the console
		PrintStream oldOut = System.out;
		PrintStream newOut = new PrintStream(new ByteArrayOutputStream());
		System.setOut(newOut);
		InputStream input = new URL(url).openStream();
		PDDocument pdDoc = PDDocument.load(input);
		PDFTextStripper textStripper = new PDFTextStripper();
		String text = textStripper.getText(pdDoc);// extract text from pdf file
		pdDoc.close();
		input.close();
		System.setOut(oldOut);
		System.out.println("completed "+url);
		return new PdfPage(url, title, text);
	}

	public static PdfPage getLocalPdfByFilePath(String filePath, String title) throws IOException {
		System.out.println("opening "+filePath);
		File file = new File(filePath);
		PDDocument pdDoc = PDDocument.load(file);
		PDFTextStripper textStripper = new PDFTextStripper();
		String text = textStripper.getText(pdDoc);
		pdDoc.close();
		System.out.println("completed "+filePath);
		return new PdfPage(filePath, title, text);
	}

	public PdfPage(String url, String title, String textData) {
		this.url = url;
		this.title = title;
		this.textData = textData;
	}

	public String getPath() {
		return this.url;
	}
	public String getTextData() {
		return this.textData;
	}
	public String getTitle() {
		return title;
	}
}
