package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
/*
 * class to represent the indexer
 */
public class LuceneIndexer {

  private Analyzer analyzer; // the way lucene analyze certain text decides whether or not to index
                             // those words
  private Directory directory;

  public LuceneIndexer(Analyzer analyzer) {
    this.analyzer = analyzer;
  }

  public LuceneIndexer(Analyzer analyzer, Directory directory){
    this.analyzer = analyzer;
    this.directory = directory;
  }

  /*
   * Takes html pages iterator and creates indexes for all the pages
   */
  public Path index(HtmlStorage htmlStorage, Path indexPath) throws IOException {
    
    Directory directory = FSDirectory.open(indexPath);
    IndexWriterConfig iwc = new IndexWriterConfig(this.analyzer);// config based on the analyzer
    IndexWriter iWriter = new IndexWriter(directory, iwc);
    writeHtmlPages(iWriter, htmlStorage);
    // close writer and dir
    iWriter.close();
    directory.close();
    return indexPath;
  }


  // public Path indexToTempDirectory(HtmlStorage htmlStorage) throws IOException {
  //   IndexWriterConfig iwc = new IndexWriterConfig(this.analyzer);// config based on the analyzer
  //   Path indexPath = Files.createTempDirectory("luceneindex");
  //   Directory directory = FSDirectory.open(indexPath);
  //   IndexWriter iWriter = new IndexWriter(directory, iwc);
  //   writeHtmlPages(iWriter, htmlStorage);
  //   // close writer and dir
  //   iWriter.close();
  //   directory.close();
  //   return indexPath;
  // }

  private IndexWriter writeHtmlPages(IndexWriter iWriter, HtmlStorage htmlStorage) throws IOException {
    for (Indexable htmlPage :htmlStorage) {
    	Document doc = new Document();
        // for now we will have two field per document
        doc.add(new Field(Fields.PATH.toString(), htmlPage.getPath(), StringField.TYPE_STORED));
        doc.add(new Field(Fields.TITLE.toString(), htmlPage.getTitle(), TextField.TYPE_STORED));
        doc.add(new Field(Fields.CONTENT.toString(), htmlPage.getTextData(), TextField.TYPE_STORED));
        // System.out.println(htmlPage.getTextData());
        // write the doc on to the index
        iWriter.addDocument(doc);
    }
    return iWriter;
  }

  // remove document that have the specified path
  public void deleteDocumentByUrl(String url, Path indexPath) throws IOException {
	  Directory directory = FSDirectory.open(indexPath);
	  IndexWriterConfig iwc = new IndexWriterConfig(new KeywordAnalyzer());
	  IndexWriter iWriter = new IndexWriter(directory, iwc);
	  
	  Term term = new Term(Fields.PATH.toString(), url);
	  // System.out.println("Deleting document with field " + term.field() + " set as " + term.text() + "...");
	  iWriter.deleteDocuments(term);
	  // System.out.println("Complete.");
	  iWriter.close();
	  directory.close();
  }
  
  // add single document to index
  public void addDocument(Indexable page, Path indexPath) throws IOException {
      Directory directory = FSDirectory.open(indexPath);
    // Directory directory = MMapDirectory.open(indexPath);
	  IndexWriterConfig iwc = new IndexWriterConfig(this.analyzer);
	  IndexWriter iWriter = new IndexWriter(directory, iwc);
	  Document doc = new Document();
	  // for now we will have two field per document
	  doc.add(new Field(Fields.PATH.toString(), page.getPath(), StringField.TYPE_STORED));
	  doc.add(new Field(Fields.TITLE.toString(), page.getTitle(), TextField.TYPE_STORED));
	  doc.add(new Field(Fields.CONTENT.toString(), page.getTextData(), TextField.TYPE_STORED));
	  // System.out.println(htmlPage.getTextData());
	  // write the doc on to the index
	  iWriter.addDocument(doc);
	  // close writer and dir
    iWriter.close();
    // directory.
    directory.close();
  }

}
