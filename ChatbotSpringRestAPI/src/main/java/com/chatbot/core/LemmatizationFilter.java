package com.chatbot.core;

import java.io.IOException;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;

public class LemmatizationFilter extends TokenFilter {

  // CharTermAttribute will contain the actual token word
  private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);

  // TypeAttribute will contain the OpenNLP POS (Treebank II) tag
  private final TypeAttribute typeAtt = addAttribute(TypeAttribute.class);
  private DictionaryLemmatizer lemmatizer;

  public LemmatizationFilter(TokenStream in, DictionaryLemmatizer lemmatizer) {
    super(in);
    this.lemmatizer = lemmatizer;
  }

  @Override
  public final boolean incrementToken() throws IOException {

    // Return false if at the end of the token stream
    if (input.incrementToken()) {
      String[] token = { termAtt.toString() };
      String[] POStag = { typeAtt.type() };

      String[] lemma = lemmatizer.lemmatize(token, POStag);

      if (!(lemma[0].equals("O"))) {
        // System.out.println(lemma[0] + " : " + termAtt.toString());

        termAtt.setEmpty();
        termAtt.copyBuffer(lemma[0].toCharArray(), 0, lemma[0].length());
      }
      return true;
    } else
      return false;
  }

}
