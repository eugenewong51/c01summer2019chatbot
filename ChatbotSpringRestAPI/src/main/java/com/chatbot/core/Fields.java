package com.chatbot.core;

public enum Fields {
  
  // Page ID/location (not indexed)
  PATH,
  
  // Page Title (indexed)
  TITLE,
  
  // Page main body of text (indexed)
  CONTENT

}
