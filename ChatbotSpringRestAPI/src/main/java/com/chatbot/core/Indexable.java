package com.chatbot.core;

public interface Indexable {
  /*
   * Return the path of the Indexable object.
   * Path -> id/location of file, does not get indexed (tokenized)
   */
  public String getPath();
  
  /*
   * Return the title of the Indexable object.
   * Title -> main descriptor of the file, gets indexed
   */
  public String getTitle();
  
  /*
   * Return the main body of text belonging to this Indexable object.
   * Text -> main component of text that will be searched on, gets indexed
   */
  public String getTextData();
}
