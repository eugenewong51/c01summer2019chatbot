package com.chatbot.core;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {
	// non tested regext for now to check url syntax before making arequest
	private String urlRegex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	private String pdfRegex = ".*\\.pdf$";
	private String baseUrl; // starting url
	private int depth_limit; // given first page is depth 0
	private Set<String> urlset; //stores set of urls
	private HtmlStorage htmlStorage; // html storage object
	private int invalidUrls = 0; // keeps count of invalid urls
	private int validLinks = 0;
	
	Crawler(String url, int depth){
		this.baseUrl = url;
		this.depth_limit = depth;
		this.urlset = new HashSet<String>();
		this.htmlStorage = new HtmlStorage();
	}
	
	public String getBaseURL(){
		return this.baseUrl;
	}

	public int getValidLinksCount(){
		return this.validLinks;
	}

	public int getInvalidLinksCount() {
		return this.invalidUrls;
	}
	
	public HtmlStorage crawl() throws ConnectionFailedException {
		// make a request using jsoup and get the document
		try {
			// first conection to test if site is available
			Jsoup.connect(baseUrl).get();
			//start crawling with depth 0
			this.extractTextOnly(this.baseUrl, 0);
			// return the html storage contating html page objects
			return this.htmlStorage;
		} catch (Exception e){
			throw new ConnectionFailedException(baseUrl);
		}	
	}
		
	private void extractTextOnly(String url, int depth){

		// check if the url is in the map
		// avoids repetative url
		if(!this.urlset.contains(url)) {
			// get the document at the base URL
			Document document;
			//System.out.println(Pattern.matches(this.urlRegex,url));
			// if it's syntactically valid url try to request the data
			if(Pattern.matches(this.urlRegex,url)) {
				// make a request using jsoup and get the document
				try {
					document = Jsoup.connect(url).get();
				} catch (Exception e){
					// log invalid url and increase invalid url count
					System.out.println(StringUtils.repeat("  ",depth)+ "INVALID (URL SYNTAX) URL skipped: " + url);
					this.invalidUrls++;
					return;
				}	
				
			} else {
				// log invalid url and increase invalid url count
				System.out.println(StringUtils.repeat("  ",depth)+ "INVALID (URL SYNTAX) URL skipped: " + url);
				this.invalidUrls++;
				return;
			}

			this.urlset.add(url);
			// create page object and add it to storage
			htmlStorage.add(new HtmlPage(document));

			// update valid links count
			this.validLinks++;

			System.out.println(StringUtils.repeat("  ",depth) + url);
			// get all links on current url
			
			// increase depth counter because now we are entering
			// next depth
			depth++;
			// check if the next depth is needed
			if(depth <= this.depth_limit) {
				Elements linksOnPage = document.select("a[href]");
				// and crawl them
				for(Element linkElement: linksOnPage ) {
					String newUrl = linkElement.attr("abs:href");
					
					// check for pdf pages
					if (Pattern.matches(this.pdfRegex,newUrl) && !this.urlset.contains(newUrl)) {
						try {
							PdfPage pdf = PdfPage.getPdfByUrl(newUrl, linkElement.text());
							this.urlset.add(newUrl);
							// create page object and add it to storage
							htmlStorage.add(pdf);
							// update valid links count
							this.validLinks++;
							
						} catch (Exception e) {
							System.out.println(StringUtils.repeat("  ",depth)+ "INVALID PDF skipped: " + newUrl);
							this.invalidUrls++;
						}
					}
					else {
						//System.out.println(newUrl);
						this.extractTextOnly(newUrl, depth);
					}
					
				}
			}	
		}

	}

//	 //some code to test the crawler class
//	 public static void main( String[] args ) throws Exception
//	    {
//	        System.out.println( "Hello World!" );
//	        Crawler c = new Crawler("http://dustyfeet.com/", 2);
//	        //Crawler c = new Crawler("https://www.utsc.utoronto.ca/~nick/cscC63/additional-notes/", 2);
//	        //Crawler c = new Crawler("http://www.google.com/", 2);
//	        c.crawl();
//	    }
}
