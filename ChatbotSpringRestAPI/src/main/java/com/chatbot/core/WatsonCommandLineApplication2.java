package com.chatbot.core;

import java.util.Scanner;
import java.util.logging.LogManager;

import com.chatbot.core.watson.WatsonManager;
import com.ibm.watson.assistant.v2.model.MessageResponse;

public class WatsonCommandLineApplication2 {

  public static void main(String[] args) {

    WatsonManager watson_manager = WatsonManager.get_Watson_Manager();
    String session_id_1 = "1";
    String session_id_2 = "2";

    Scanner queryScanner = new Scanner(System.in);
    // Suppress log messages in stdout.
    LogManager.getLogManager().reset();

    // Initialize with empty value to start the conversation.
    String inputText = "";

    MessageResponse response = watson_manager.send_message(session_id_1, inputText);
    watson_manager.send_message(session_id_2, inputText);
    
    watson_manager.send_message(session_id_1, "index");
    watson_manager.send_message(session_id_2, "index");
    watson_manager.send_message(session_id_1, "website");

    // Prompt for next round of input.
    System.out.print(">> ");
    inputText = queryScanner.nextLine();



    watson_manager.delete_session(session_id_1);
    watson_manager.delete_session(session_id_2);
    queryScanner.close();
  }

}
