package com.chatbot.core;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * class to represent collection of
 * crawled html pages
 */
public class HtmlStorage implements Iterable<Indexable>{
	private ArrayList<Indexable> htmlPages = new ArrayList<Indexable>();

	public void add(Indexable page) {
		this.htmlPages.add(page);
	}
	
	public ArrayList<Indexable> getHtmlPages(){
		return this.htmlPages;
	}
	
	public Iterator<Indexable> iterator() {
		return htmlPages.iterator();
	}

	public int getNumDoc(){
		return htmlPages.size();
	}

}
