package com.chatbot.core;

public class IndexExistsException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5311075091384817112L;

	public IndexExistsException(String indexName){
		super("Can't store duplicate index " + indexName);
	}
}
