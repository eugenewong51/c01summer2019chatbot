package com.chatbot.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.ibm.cloud.objectstorage.ClientConfiguration;
import com.ibm.cloud.objectstorage.SDKGlobalConfiguration;
import com.ibm.cloud.objectstorage.auth.AWSCredentials;
import com.ibm.cloud.objectstorage.auth.AWSStaticCredentialsProvider;
import com.ibm.cloud.objectstorage.auth.BasicAWSCredentials;
import com.ibm.cloud.objectstorage.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3ClientBuilder;
import com.ibm.cloud.objectstorage.services.s3.model.Bucket;
import com.ibm.cloud.objectstorage.services.s3.model.DeleteObjectsRequest;
import com.ibm.cloud.objectstorage.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.ibm.cloud.objectstorage.services.s3.model.DeleteObjectsResult;
import com.ibm.cloud.objectstorage.services.s3.model.GetObjectRequest;
import com.ibm.cloud.objectstorage.services.s3.model.ListObjectsRequest;
import com.ibm.cloud.objectstorage.services.s3.model.ListObjectsV2Request;
import com.ibm.cloud.objectstorage.services.s3.model.ListObjectsV2Result;
import com.ibm.cloud.objectstorage.services.s3.model.ObjectListing;
import com.ibm.cloud.objectstorage.services.s3.model.ObjectMetadata;
import com.ibm.cloud.objectstorage.services.s3.model.PutObjectRequest;
import com.ibm.cloud.objectstorage.services.s3.model.S3Object;
import com.ibm.cloud.objectstorage.services.s3.model.S3ObjectInputStream;
import com.ibm.cloud.objectstorage.services.s3.model.S3ObjectSummary;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.MMapDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.el.stream.Stream;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.IndexInput;

import com.ibm.cloud.objectstorage.oauth.BasicIBMOAuthCredentials;

public final class LuceneIndexStorageManager implements IndexStorage{
    
	// private static AmazonS3 _cosClient;
    String api_key = "_8PW2_-79pqYgxbwc5hts9R_wM5uTlSbGguxNqequ29J"; // eg "W00YiRnLW4a3fTjMB-oiB-2ySfTrFBIQQWanc--P3byk"
    String service_instance_id = "crn:v1:bluemix:public:cloud-object-storage:global:a/24099efc858d4501a037a06ec8313b27:0855bc0c-b386-4f55-b37e-6bce45a642a5::"; // eg "crn:v1:bluemix:public:cloud-object-storage:global:a/3bf0d9003abfb5d29761c3e97696b71c:d6f04d83-6c4f-4a62-a165-696756d63903::"
    String endpoint_url = "s3.us-south.cloud-object-storage.appdomain.cloud"; // this could be any service endpoint
    private static AmazonS3 _cosClient;
    String storageClass = "us-east-standard";
    String location = "us-east"; 
    Logger logger = LoggerFactory.getLogger(LuceneIndexStorageManager.class);


    public LuceneIndexStorageManager(){
        this.createClient(api_key, service_instance_id, endpoint_url, location);
        System.out.println("Client connection established...");
    }
    
    @Override
    public Path retrive(String bucketName) throws IOException {
    	Path indexPath = Files.createTempDirectory(bucketName);
//    	indexPath.
    	// ListObjectsV2Result result = getBucketItems(bucketName);
    	
    	List<S3ObjectSummary> objects = getObjectSummaryList(bucketName);
    	
    	for(S3ObjectSummary obj: objects) {
    		String fname = obj.getKey();
    		File file = new File(indexPath.toString() + "/" + fname);
    		GetObjectRequest request = new GetObjectRequest(bucketName, fname);
    		_cosClient.getObject(request,file);
    	}	
    	return indexPath;
    }
    
    /**
     * @param bucketName
     * @param clientNum
     * @param api_key
     * @param service_instance_id
     * @param endpoint_url
     * @param location
     * @return AmazonS3
     */
    private void createClient(String api_key, String service_instance_id, String endpoint_url, String location)
    {
        AWSCredentials credentials;
        credentials = new BasicIBMOAuthCredentials(api_key, service_instance_id);

        ClientConfiguration clientConfig = new ClientConfiguration().withRequestTimeout(5000);
        clientConfig.setUseTcpKeepAlive(true);

        _cosClient = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withEndpointConfiguration(new EndpointConfiguration(endpoint_url, location)).withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfig).build();
    }

	@Override
	public void store(String indexName, Path path) throws IndexExistsException {
		 File directory = path.toFile();
        // create a new bucket under buckateName if it doesnt exist
	 	if(_cosClient.doesBucketExistV2(indexName)) {
	 		throw new IndexExistsException(indexName);
	 	}
	 	
	 	_cosClient.createBucket(indexName);
        
        // for the given dir upload all its file to the bucket
        String[] allFiles = directory.list();
        
       if(allFiles.length == 0) {
    	   return;
       }
	        
        for(int i=0; i <allFiles.length; i++){
            String fname = allFiles[i];
            File file =path.resolve(fname).toFile();

            PutObjectRequest req = new PutObjectRequest(indexName, fname , file);
            ObjectMetadata metadata = new ObjectMetadata(); // define the metadata
            metadata.setContentType("application/octet-stream"); // set the metadata
            metadata.setContentLength(file.length());
            req.setMetadata(metadata);
            _cosClient.putObject(req);
        }
        return;
		
	}


	@Override
	public void delete(String indexName) throws NoSuchIndexException {
		this.deleteAllBucketContents(indexName);
		_cosClient.deleteBucket(indexName);
		
	}

	private List<S3ObjectSummary> getObjectSummaryList(String bucketName){
		// get list of all items in the bucket
	    ObjectListing objectListing = _cosClient
				.listObjects(new ListObjectsRequest().withBucketName(bucketName));
				
		return objectListing.getObjectSummaries();
	}


	private void deleteAllBucketContents(String indexName) throws NoSuchIndexException{
		// check if such index exixsts
		if(!_cosClient.doesBucketExistV2(indexName)) {
			throw new NoSuchIndexException(indexName);
	   }

	   // initialize list all file names
	   List<KeyVersion> fileNames = new ArrayList<KeyVersion>();
	   
	   for (S3ObjectSummary objectSummary :getObjectSummaryList(indexName)) {
		   fileNames.add(new KeyVersion(objectSummary.getKey()));
	   }
	   // create a delete request with list of all files to be deleted
	   DeleteObjectsRequest req = new DeleteObjectsRequest(indexName);
	   req.withKeys(fileNames);
	   // get deletion result
	   DeleteObjectsResult res = _cosClient.deleteObjects(req);
//
	   List<DeleteObjectsResult.DeletedObject> deletedItems = res.getDeletedObjects();
	   for(DeleteObjectsResult.DeletedObject deletedItem : deletedItems) {
		   System.out.printf("Deleted item: %s\n", deletedItem.getKey());
	   }
	}

	@Override
	public void update(String indexName, Path path) throws NoSuchIndexException {
		
		this.deleteAllBucketContents(indexName);
		
		File directory = path.toFile();
		// for the given dir upload all its file to the bucket
        String[] allFiles = directory.list();
        
       if(allFiles.length == 0) {
    	   return;
       }
	        
        for(int i=0; i <allFiles.length; i++){
            String fname = allFiles[i];
            File file =path.resolve(fname).toFile();

            PutObjectRequest preq = new PutObjectRequest(indexName, fname , file);
            ObjectMetadata metadata = new ObjectMetadata(); // define the metadata
            metadata.setContentType("application/octet-stream"); // set the metadata
            metadata.setContentLength(file.length());
            preq.setMetadata(metadata);
            _cosClient.putObject(preq);
        }
	}
}