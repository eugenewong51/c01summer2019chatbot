package com.chatbot.core;

public class ConnectionFailedException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3284425984216091985L;

	public ConnectionFailedException(String url){
		super("Error connecting the url " + url);
	}
}
