package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/*
 * class to represent the searcher
 */
public class LuceneSearcher {

  Analyzer analyzer;
  private int resultAmount = 10;

  public LuceneSearcher(Analyzer analyzer) {
    this.analyzer = analyzer;
  }
  // change the amount of result shown in search or result highlight
  public boolean changeResultAmount(int resultAmount) {
	  if (resultAmount <= 0) {
		  return false;
	  }
	  else {
		  this.resultAmount = resultAmount;
		  return true;
	  }
  }
  public int getResultAmount() {
	  return resultAmount;
  }

  /*
   * given a path to built index and a search query search return the list of urls
   * of the docs with relevance
   */
  public ArrayList<Document> search(Path indexPath, String userQuery)
      throws IOException, ParseException {
    
    Directory directory = FSDirectory.open(indexPath); // location of the index
    DirectoryReader ireader = DirectoryReader.open(directory); // read the dir
    IndexSearcher isearcher = new IndexSearcher(ireader); // read the dir to search from it

    QueryParser parser = new QueryParser(Fields.CONTENT.name(), analyzer); // create the parser
    Query query = parser.parse(userQuery); // pass in a query
    TopDocs topDocs = isearcher.search(query, resultAmount, Sort.RELEVANCE); // tops docs is the result set
    ScoreDoc[] hits = topDocs.scoreDocs;

    ArrayList<Document> hitDoc = new  ArrayList<Document>(); 
    for (ScoreDoc hit : hits) {
      hitDoc.add(isearcher.doc(hit.doc)); // get one doc
    }
    ireader.close();
    directory.close();

    return hitDoc;
  }
  // search for documents by specified field and get a list of matching docs
  public ArrayList<Document> search(Path indexPath, String userQuery, Fields field)
      throws IOException, ParseException {
   
    Directory directory = FSDirectory.open(indexPath); // location of the index
    DirectoryReader ireader = DirectoryReader.open(directory); // read the dir
    IndexSearcher isearcher = new IndexSearcher(ireader); // read the dir to search from it

    QueryParser parser = new QueryParser(field.toString(), analyzer); // create the parser
    Query query = parser.parse(userQuery); // pass in a query
    TopDocs topDocs = isearcher.search(query, resultAmount, Sort.RELEVANCE, true); // tops docs is the result set
    ScoreDoc[] hits = topDocs.scoreDocs;
    
    ArrayList<Document> hitDoc = new  ArrayList<Document>(); 
    for (ScoreDoc hit : hits) {
      //System.out.println(hit.score);
      if(hit.score > 0.15)
        hitDoc.add(isearcher.doc(hit.doc)); // get one doc
    }
    ireader.close();
    directory.close();

    return hitDoc;
  }
  // get a list of result highlight
  public ArrayList<String> resultHighlight(Path indexPath, String userQuery, Fields field)
		  throws IOException, ParseException, InvalidTokenOffsetsException {

	  Directory directory = FSDirectory.open(indexPath); // location of the index
	  DirectoryReader ireader = DirectoryReader.open(directory); // read the dir
	  IndexSearcher isearcher = new IndexSearcher(ireader); // read the dir to search from it

	  QueryParser parser = new QueryParser(field.toString(), analyzer); // create the parser
	  Query query = parser.parse(userQuery); // pass in a query

	  QueryScorer queryScorer = new QueryScorer(query, field.toString());
	  Fragmenter fragmenter = new SimpleSpanFragmenter(queryScorer);// determine how will the text be broken into multiple fragments, you can add an int for fragment size
	  Highlighter highlighter = new Highlighter(new SimpleHTMLFormatter("*", "*"), queryScorer); // Set the best scorer fragments
	  highlighter.setTextFragmenter(fragmenter); 


	  TopDocs topDocs = isearcher.search(query, resultAmount, Sort.RELEVANCE); // tops docs is the result set
	  ScoreDoc[] hits = topDocs.scoreDocs;

	  ArrayList<Document> hitDoc = new ArrayList<Document>();
	  ArrayList<String> resultHighlight = new ArrayList<String>();
	  for (int i = 0; i < hits.length; i++) {
		  Document doc = isearcher.doc(hits[i].doc);
		  hitDoc.add(doc);
		  String text = doc.get(Fields.CONTENT.toString());
		  String fragment = highlighter.getBestFragment(analyzer, field.toString(), text);
		  resultHighlight.add(fragment);
	  }
	  ireader.close();
	  directory.close();

	  return resultHighlight;
  }
  
  @SuppressWarnings("unchecked")
public String jsonOutput(Path indexPath, String userQuery, Fields field) throws IOException, ParseException, InvalidTokenOffsetsException {
	  JSONArray resultArr = new JSONArray();
    JSONObject result, resultFields;
	  ArrayList<Document> hitDoc = search(indexPath, userQuery, field);
	  ArrayList<String> resultHighlight = resultHighlight(indexPath, userQuery, field);
	  // go thorugh each result and extract their fields and put it in the json list
	  for (int i = 0; i<hitDoc.size();i++) {
		  resultFields = new JSONObject();
		  Document doc = hitDoc.get(i);
		  resultFields.put("url", doc.get(Fields.PATH.toString()));
		  resultFields.put("title", doc.get(Fields.TITLE.toString()));
		  resultFields.put("HighLight", resultHighlight.get(i));
		  
		  result = new JSONObject();
		  result.put("result", resultFields);
		  resultArr.add(result);
	  }
	  // create JSON string
      return resultArr.toJSONString();
  }
  
  
}
