package com.chatbot.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class LuceneDriver {
	private static LuceneDriver driver = null;
	// private Path indexPath;
	private static LuceneIndexer indexer;
	private static Crawler crawler; 
	private static LuceneSearcher searcher;
	private static IndexStorage storage = new LuceneIndexStorageManager();

	// get driver object
	public static LuceneDriver getDriver(String url, int depth) {
		if (driver == null){
			driver = new LuceneDriver(url, depth);
		}
		return driver;
	}
//	// initialize driver
//	private LuceneDriver(String url, int depth, Path indexPath) throws IOException, ConnectionFailedException {
//		// this.indexPath = indexPath;
//		crawler = new Crawler(url, depth);
//		indexer = new LuceneIndexer(new StandardAnalyzer());
//		indexer.index(crawler.crawl(), indexPath);
//		searcher = new LuceneSearcher(new StandardAnalyzer());
//	}
	
	
	// initialize driver
	private LuceneDriver(String url, int depth)  {
		// this.indexPath = indexPath;
		crawler = new Crawler(url, depth);
		indexer = new LuceneIndexer(new StandardAnalyzer());
		searcher = new LuceneSearcher(new StandardAnalyzer());
	}
	

	// // index a given website and save the index to a given path location
	// public static JSONObject indexWebsite(String url, int depth, Path indexPath) throws ConnectionFailedException {
		
	// 	// JSONObject result = new JSONObject();
	// 	HashMap<String,Object> resultFields = new HashMap<String,Object>();
	// 	crawler = new Crawler(url, depth);
	// 	indexer = new LuceneIndexer(new StandardAnalyzer());
	// 	HtmlStorage htmlStroage = crawler.crawl();
	// 	try {
	// 		indexer.index(htmlStroage, indexPath);
	// 	} catch (IOException e) {
	// 		// TODO Auto-generated catch block
	// 		e.printStackTrace();
	// 		resultFields.put("status", "failure");
	// 		resultFields.put("url", crawler.getBaseURL());
	// 	}
	// 	  resultFields.put("status", "success");
	// 	  resultFields.put("url", crawler.getBaseURL());
	// 	  resultFields.put("validCount", crawler.getValidLinksCount());
	// 	  resultFields.put("invalidCount", crawler.getInvalidLinksCount());
	// 	  resultFields.put("location", indexPath.toString());

	// 	  JSONObject result  = new JSONObject(resultFields);
	// 	//   result.put("result", resultFields);
	// 	  return result;
	// }
	
	// create a new index and save it to storage
	public static Iterator<Indexable> index(String url, int depth, String key) 
				throws ConnectionFailedException, IOException, IndexExistsException {
		
		crawler = new Crawler(url, depth);
		indexer = new LuceneIndexer(new StandardAnalyzer());
		HtmlStorage htmlStorage = crawler.crawl();
		Path indexPath = Files.createTempDirectory("luceneindex");
		Path path = indexer.index(htmlStorage, indexPath);
		storage.store(key,path); // also store it in the storage
		return htmlStorage.iterator();
	}
	
	// update an index that's in the storage
	public static Iterator<Indexable> update(String url, int depth, String indexName)
			throws ConnectionFailedException, IOException, NoSuchIndexException {
		
		// initialize the crawler and the indexer
		crawler = new Crawler(url, depth);
		indexer = new LuceneIndexer(new StandardAnalyzer());
		// crawl the site
		HtmlStorage htmlStorage = crawler.crawl();
		// grab the index from the storage and index new pages
		Path indexPath = storage.retrive(indexName);
		indexer.index(htmlStorage, indexPath);
		// update the files in the storage
		storage.update(indexName, indexPath);
		return htmlStorage.iterator();
	}

	// delete an indexable from an index
	public static void deleteDocument(String url, String indexName) 
					throws IOException, NoSuchIndexException {
		// grab the index from the storage and index new pages
		Path indexPath = storage.retrive(indexName);
		indexer.deleteDocumentByUrl(url, indexPath);
		// update the files in the storage
		storage.update(indexName, indexPath);
	}

	// delete an indexable from an index
	public static void delete(String indexName) throws NoSuchIndexException {
		storage.delete(indexName);
	}



	//search given a query
	// public static String search(String query, Path indexPath) {
	// 	String result = "";
	// 	searcher = new LuceneSearcher(new StandardAnalyzer());
	// 	try {
	// 		result =  searcher.jsonOutput(indexPath, query, Fields.CONTENT);
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
			
	// 	}
	// 	return result;
	// }
	
	// search given a query
	public static String search(String query) throws IOException {
		String result = "";

		LuceneIndex.getInstance();
		Path indexPath = LuceneIndex.getIndexPath();

		searcher = new LuceneSearcher(new StandardAnalyzer());
		// File indexDir = new File(indexPath);
		try {
			result =  searcher.jsonOutput(indexPath, query, Fields.CONTENT);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}


	// get the path where the index is stored
	// public Path getIndexPath() {
	// 	return indexPath;
	// }
	// public boolean addPage(String url, String title, Path indexPath) {
	// 	if (url.matches("\\.pdf$")) {
	// 		Indexable page;
	// 		try {
	// 			page = PdfPage.getPdfByUrl(url, title);
	// 			indexer.addDocument(page, indexPath);
	// 			return true;
	// 		} catch (Exception e) {
	// 			// TODO Auto-generated catch block
	// 			return false;
	// 		}
	// 	}
	// 	else {
	// 		Document document = null;
	// 		try {
	// 			// get content with the given url
	// 			document = Jsoup.connect(url).get();
	// 			indexer.addDocument(new HtmlPage(document), indexPath);
	// 			return true;
	// 		} catch (Exception e) {
	// 			return false;
	// 		}
	// 	}
	// }
	



	// public boolean deletePage(String url, Path indexPath) {
	// 	try {
	// 		indexer.deleteDocumentByUrl(url, indexPath);
	// 		return true;
	// 	} catch (Exception e) {
	// 		// TODO Auto-generated catch block
	// 		return false;
	// 	}
	// }

}
