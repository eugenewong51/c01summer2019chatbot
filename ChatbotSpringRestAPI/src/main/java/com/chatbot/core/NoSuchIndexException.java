package com.chatbot.core;

public class NoSuchIndexException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4194687977539780820L;

	public NoSuchIndexException(String indexName){
		super("No index under the name: " + indexName);
	}
}
