package com.chatbot.core;

import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;

/*
 * class to hold an html page
 * this will expan in future to represent an html page
 * and a lot of its components. which me make it easy to build accurate index
 * for specific tags
 */
public class HtmlPage implements Indexable{
	// might use builder design pattern
	private String url;
	private String textData; // this is a single string containing
	private String title = "";
	//table = "", images = "", headers = "", paragraphs = "";
//	private Map<String, String> fields;
	private final String[] exclude = {"nav", "footer", "header"};

	public HtmlPage(String url, String title, String textData) {
		this.url = url;
		this.title = title;
		this.textData = textData;
	}
	
	public HtmlPage(Document document) {
		this.url = document.baseUri();
		//this.textData = document.text();
		//fields = new HashMap<String, String>();
		htmlSetUp(document);
	}
	
//	public void addField(Document doc, String tag) {
//		// add field to this HTMLPage
//		String content = collectContent(doc, tag);
//		fields.put(tag, content);
//	}
//	public void addField(Document doc, String tag, String[] attrs) {
//		// add field that contains attribute to this HTMLPage
//		String content = collectContent(doc, tag, attrs);
//		fields.put(tag+Arrays.toString(attrs), content);
//	}
//	public String getContent(String tag) {
//		return fields.get(tag);
//	}
//	public String getContent(String tag, String[] attrs) {
//		return fields.get(tag+Arrays.toString(attrs));
//	}
	
	private void htmlSetUp(Document document) {
		// get title of the page
		this.title = document.title();
		// remove the tags that should not be in the content
		Document cloneDoc = document.clone();
		for (String tag: exclude) {
			cloneDoc.select(tag).remove();
		}
		this.textData = cloneDoc.text();
		
//		// get keywords from table
//        this.table = collectContent(document, "th");
//        table += collectContent(document, "caption");
//		// get image alternate text
//        String[] attrArr = {"alt"};
//		this.images = collectContent(document, "img", attrArr);
//		// get header
//		// this.headers = collectContent(document, "h0, h1, h2, h3, h4, h5, h6");
//		// get content from paragraph
//		// this.paragraphs=collectContent(document, "p");
	}
	
//	private String collectContent(Document doc, String tag) {
//		// collect content in specific tags
//		return doc.select(tag).text();
//	}
//	private String collectContent(Document doc, String tag, String[] attrs) {
//		// collect attributes in specific tags
//		String keywords = "";
//		for (String attr:attrs) {
//			tag +="["+attr+"]";
//		}
//		Elements elements = doc.select(tag);
//        for (Element element : elements) {
//    		for (String attr:attrs) {
//    			keywords += " " + element.attr(attr);
//    		}
//        }
//		return keywords;
//	}

	
	public String getPath() {
		return this.url;
	}
	public String getTextData() {
		return this.textData;
	}
	public String getTitle() {
		return title;
	}
//	public String getTable() {
//		return table;
//	}
//	public String getImages() {
//		return images;
//	}
//	public String getHeaders() {
//		return headers;
//	}
//	public String getParagraphs() {
//		return paragraphs;
//	}


}
