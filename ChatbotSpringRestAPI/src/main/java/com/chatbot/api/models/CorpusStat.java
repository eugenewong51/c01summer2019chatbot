package com.chatbot.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CORPUS_STATS")
public class CorpusStat {

  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE)
  private int ID;
  
  @Column(unique=true, nullable = false)
  private String questionId;
  private String question;
  private int TopDocCount;
  private int RelevantDocCount;
  private double AvgConfidence;

  public int getID() {
    return ID;
  }

  public void setID(int id) {
    ID = id;
  }

  public String getQuestionID() {
    return questionId;
  }

  public void setQuestionID(String questionId) {
    this.questionId = questionId;
  }

  public String getQuestion() {
    return this.question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public int getTopDocCount() {
    return TopDocCount;
  }

  public void setTopDocCount(int topDocCount) {
    this.TopDocCount = topDocCount;
  }

  public int getRelevantDocCount() {
    return this.RelevantDocCount;
  }

  public void setRelevantDocCount(int RelevantDocCount) {
    this.RelevantDocCount = RelevantDocCount;
  }

  public double getAvgConfidence() {
    return this.AvgConfidence;
  }

  public void setAvgConfidence(double AvgConfidence) {
    this.AvgConfidence = AvgConfidence;
  }
 
  public CorpusStat incrementTopDocCount(){
    this.TopDocCount += 1;
    return this; 
  }

  public CorpusStat incrementRelevantDocCount(){
    this.RelevantDocCount += 1;
    return this; 
  }

  public CorpusStat updateAverageConfidence(double newConfidence){
    double old_avg_confidence = this.getAvgConfidence();
    int total_relevant_search_count = this.getRelevantDocCount();
    double new_avg_confidence = (old_avg_confidence + newConfidence) / total_relevant_search_count; 

    this.AvgConfidence = new_avg_confidence; 
    return this; 
  }
}
