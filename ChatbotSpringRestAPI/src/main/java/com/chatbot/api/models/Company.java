package com.chatbot.api.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Company {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int ID;
	
	private String CompanyName;

	@OneToMany( fetch = FetchType.LAZY)
	@JoinColumn
	private List<Resources> resources;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	// public List<Resources> getResources() {
	// 	return resources;
	// }

	public void setResources(List<Resources> resources) {
		this.resources = resources;
	}

}
