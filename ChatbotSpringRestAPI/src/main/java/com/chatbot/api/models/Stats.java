package com.chatbot.api.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
public class Stats {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	
	private String sessionId;

	@Basic(optional = false)
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date date = new DateTime().toDate();

	@ManyToOne
	private BotUser user;
	
	private int MessageCount;
	
	private int NoResponseCount;

	public int getId() {
		return id;
	}

	public void setId(int statsId) {
		this.id = statsId;
	}

	public String getSessionId() {
		return sessionId;
	}
	
	public BotUser getUser() {
		return user;
	}

	public void setUser(BotUser user) {
		this.user = user;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getMessageCount() {
		return MessageCount;
	}

	public void setMessageCount(int messageCount) {
		MessageCount = messageCount;
	}

	public int getNoResponseCount() {
		return NoResponseCount;
	}

	public void setNoResponseCount(int noResponseCount) {
		NoResponseCount = noResponseCount;
	}
	/*
	public Stats incrementMessageCount() {
		this.MessageCount += 1;
		return this;
	}
	
	public Stats incrementNoResponseCount() {
		this.NoResponseCount += 1;
		return this;
	}
	*/
	
}
