package com.chatbot.api.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.hibernate.annotations.CreationTimestamp;
import org.joda.time.DateTime;

import java.util.Date;

import javax.persistence.*;

@Entity
public class Resources {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int ID;

	private String name;
	
	@ManyToOne
	private Company company;

	private String origin;
	
	@Basic(optional = false)
	@CreationTimestamp
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt = new DateTime().toDate();
	// private LocalDateTime addedOn;

	private int depth;

//	private int validUrls;
//
//	private int invalidUrls;

	private boolean isDefault;

	public int getID() {
		return ID;
	}

//	public int getInvalidUrls() {
//		return invalidUrls;
//	}
//
//	public void setInvalidUrls(int invalidUrls) {
//		this.invalidUrls = invalidUrls;
//	}
//
//	public int getValidUrls() {
//		return validUrls;
//	}
//
//	public void setValidUrls(int validUrls) {
//		this.validUrls = validUrls;
//	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	// public void setAddedOn(LocalDateTime addedOn) {
	// 	this.addedOn = addedOn;
	// }

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setID(int iD) {
		ID = iD;
	}

	

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
