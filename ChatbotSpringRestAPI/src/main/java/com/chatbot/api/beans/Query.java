package com.chatbot.api.beans;

/*
 * query request that is sent from the
 * front end, this may extend a base 'request' class
 * that we can use to track user in each request
 */
public class Query {
	private String query;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
