package com.chatbot.api.beans;

import java.util.Date;


/*
 * bean for incoming corpus doc
 * for dfi
 */
public class DFICorpusDocument {
	
	private String[] question;
	
	private String[] answer;
	
	private String author;
	
	private Date date;

	public String[] getQuestion() {
		return question;
	}

	public void setQuestion(String[] question) {
		this.question = question;
	}

	public String[] getAnswer() {
		return answer;
	}

	public void setAnswer(String[] answer) {
		this.answer = answer;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
}
