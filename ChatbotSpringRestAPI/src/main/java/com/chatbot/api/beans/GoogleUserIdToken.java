package com.chatbot.api.beans;


/*
 * class to represent token obtained from
 * the frontend to verify google user id token
 * integrity
 */
public class GoogleUserIdToken {
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
