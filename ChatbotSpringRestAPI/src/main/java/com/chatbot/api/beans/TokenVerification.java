package com.chatbot.api.beans;


/*
 * class to represent the verification result
 * once a google token's integrity is checked
 */
public class TokenVerification {
	private boolean isValid;
	private int userId;
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
}
