package com.chatbot.api.beans;

/*
 * a reply object eventually this object
 * will have more fields that representing 
 * several attributes of a resposnse to a query
 */
public class QueryReply {
	private String query;
	private String reply;
	private String url;
	
	public String getQuery() {
		return query;
	}
	
	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
