package com.chatbot.api.sessionManagement;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringSessionController {

	@GetMapping(path="/list-session")
	public List<String> process(Model model, HttpSession session) {
		@SuppressWarnings("unchecked")
		List<String> messages = (List<String>) session.getAttribute("chatbot");
		System.out.println(messages);
		if (messages == null) {
			messages = new ArrayList<>();
		}
		
		return messages;
	}

//	@PostMapping(path="/persistMessage")
//	public List<String> persistMessage(@RequestParam("msg") String msg, HttpServletRequest request) {
//		@SuppressWarnings("unchecked")
//		List<String> messages = (List<String>) request.getSession().getAttribute("chatbot");
//		if (messages == null) {
//			messages = new ArrayList<>();
//			request.getSession().setAttribute("chatbot", messages);
//		}
//		messages.add(msg);
//		request.getSession().setAttribute("MY_SESSION_MESSAGES", messages);
//		System.out.println(request.getSession().getId());
//		System.out.println(request.getSession().getAttribute("user"));
//		return messages;
//	}

	@PostMapping(path="/destroy-session")
	public String destroySession(HttpServletRequest request) {
		request.getSession().invalidate();
		return "session destroyed";
	}
}