package com.chatbot.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.api.controllers.UserNotFoundException;
import com.chatbot.api.models.BotUser;
import com.chatbot.api.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repo;
	
	public BotUser addUser(BotUser newUser) {
		repo.save(newUser);
		return newUser;
	}
	
	public List<BotUser> getAllUsers() {
		List<BotUser> users = (List<BotUser>) repo.findAll();
		return users;
	}
	
	public BotUser getUserById(int id) {
		return repo.findById(id)
				.orElseThrow(()-> new UserNotFoundException(id));	
		}
	
	public void deleteUser(int id) {
		repo.deleteById(id);
	}
}
