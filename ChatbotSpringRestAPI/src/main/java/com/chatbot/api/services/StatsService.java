package com.chatbot.api.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.api.controllers.StatNotFoundException;
import com.chatbot.api.models.Stats;
import com.chatbot.api.repository.StatsRepository;

@Service
public class StatsService {

	@Autowired
	private StatsRepository repo;
	
	public Stats addStat(Stats stats) {
		return repo.save(stats);
	}
	
	// returns the list of all stats
	public Iterable<Stats> getAllStats() {
		return repo.findAll();
	}
	
	// return a statistic based on SessionId
	public Stats getStatsBySessionId(String sessionId) {
		return repo.findBySessionId(sessionId); 
	}
	
	// returns the selected statistic
	public Stats getStatsById(int id) {
		return repo.findById(id)
				.orElseThrow(()-> new StatNotFoundException(id));
	}

	// returns the selected statistic by user Id
	public List<Stats> getStatsByUserId(int user) {
		return repo.findByUserID(user);
	}

	// delete a statistic
	public void deleteStat(int id) {
		repo.deleteById(id);
	}
	
	public Stats updateNoResponseCount(String sessionId) {
		repo.updateNoResponseCount(sessionId);
		return repo.findBySessionId(sessionId);
	}
	
	public Stats updateMessageCount(String sessionId) {
		repo.updateMessageCount(sessionId);
		return repo.findBySessionId(sessionId);
	}
	
	// get the stats within a date range
	public List<Stats> getStatsByDateRange(Date start, Date end) {
		return repo.findByDateRange(start, end);
	}
}
