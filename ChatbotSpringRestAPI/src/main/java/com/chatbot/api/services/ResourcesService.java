package com.chatbot.api.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.api.controllers.DefaultResourceNotFound;
import com.chatbot.api.controllers.ResourceNotFoundException;
import com.chatbot.api.models.Company;
import com.chatbot.api.models.Resources;
import com.chatbot.api.repository.ResourcesRepository;
import com.chatbot.core.ConnectionFailedException;
import com.chatbot.core.IndexExistsException;
import com.chatbot.core.Indexable;
import com.chatbot.core.LuceneDriver;
import com.chatbot.core.NoSuchIndexException;

@Service
public class ResourcesService {

	@Autowired
	private ResourcesRepository resourcesRepository;

	@Autowired
	private CompanyService companyService;

	Logger logger = LoggerFactory.getLogger(ResourcesService.class);

	// adds a resource into the database given the resource location and company
	// the ID of the resource is auto incremented in the database
	public Resources addAResource(Resources resource){
		return resourcesRepository.save(resource);
	}


	// adds resources given the url and company
	// the ID of the resource is auto incremented in the database
	public Iterable<Resources> addResourceOnCrawlRequest(Resources resource) 
	                    throws ConnectionFailedException,IOException, IndexExistsException{
		// check if the company exists
		System.out.println(resource.getName());
		Company company = companyService.getCompanyById(resource.getCompany().getID());
		//set company
		resource.setCompany(company);
		// remove in appropriate characters 
		resource.setName(resource.getName().replaceAll("[^a-zA-Z0-9]", "").toLowerCase());
		// index the url and get response from indexing
		Iterator<Indexable> indexables = null;
		try {
			logger.info("Crawling the resource");
			indexables = LuceneDriver.index(resource.getOrigin(),
			                      resource.getDepth(),resource.getName());
		} catch (ConnectionFailedException |IOException |IndexExistsException e){
			logger.error("Error in adding a crawled resource", e);;
			throw e;
		}
		List<Resources> resources = new ArrayList<Resources>();
		System.out.println("before iterator");
		// save all pages as separate resources
		while(indexables.hasNext()) {
			Indexable res = indexables.next();
			Resources newResource = new Resources();
			newResource.setOrigin(res.getPath());
			System.out.println(res.getPath());
			newResource.setCompany(resource.getCompany());
			System.out.println(resource.getCompany());
			newResource.setName(resource.getName());
			resources.add(newResource);
		
		}	
		
		return resourcesRepository.saveAll(resources);
	}
	// adds resources given the url and company
	// the ID of the resource is auto incremented in the database
	public Iterable<Resources> updateResourceOnCrawlRequest(Resources resource)
	                    throws ConnectionFailedException,IOException, NoSuchIndexException{
		// check if the company exists
		System.out.println(resource.getName());
		Company company = companyService.getCompanyById(resource.getCompany().getID());
		//set company
		resource.setCompany(company);
		// index the url and get response from indexing
		Iterator<Indexable> indexables = null;
		try {
			logger.info("Crawling the resources to update index for " + resource.getName());
			indexables = LuceneDriver.update(resource.getOrigin(),
			                      resource.getDepth(),resource.getName());
		} catch (ConnectionFailedException |IOException |NoSuchIndexException e){
			logger.error("Error in updating the resource for " + resource.getName(), e);;
			throw e;
		}
		List<Resources> resources = new ArrayList<Resources>();
		// save all pages as separate resources
		while(indexables.hasNext()) {
			Indexable res = indexables.next();
			Resources newResource = new Resources();
			newResource.setOrigin(res.getPath());
			System.out.println(res.getPath());
			newResource.setCompany(resource.getCompany());
			System.out.println(resource.getCompany());
			newResource.setName(resource.getName());
			resources.add(newResource);
		
		}	
		return resourcesRepository.saveAll(resources);
	}

	public void deleteResourceGroup(String name) throws NoSuchIndexException {
		LuceneDriver.delete(name);
		resourcesRepository.deleteResourceGroup(name);
	}


	// returns the list of all resources
	public Iterable<Resources> getAllResources() {
		return resourcesRepository.findAll();
	}
	
	public void markAllDefault(String name){
		resourcesRepository.markAllNotDefault();
		resourcesRepository.markAllDefault(name);
		return ;
	}

	public String getDefaultResource(){
		List<Resources> res = resourcesRepository.findAllByIsDefault();
		if(res.size()>0){
			return res.get(0).getName();
		}
	    throw new NoDefaultResourceException();
		
	}

	// returns the a selected resource
	public Resources getResourcesById(int id) {
		return resourcesRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException(id));
	}
	
	// returns the a selected resource
	public List<Resources> getResourcesByCompany(int companyId) {
		return resourcesRepository.findByCompany(companyId);
	}
	
	// delete a resource
	public void deleteResource(int id) throws IOException, NoSuchIndexException {
		Resources r = resourcesRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException(id));
		logger.info("Deleting resource for: " + r.getName() + " under " + r.getOrigin());
		LuceneDriver.deleteDocument(r.getOrigin(), r.getName());
		resourcesRepository.deleteById(r.getID());
	}


	public static void main(String[] args){
		System.out.println("testing resource service...");
		

	}
}
