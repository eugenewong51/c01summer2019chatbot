package com.chatbot.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.api.controllers.CompanyNotFoundException;
import com.chatbot.api.models.Company;
import com.chatbot.api.repository.CompanyRepository;

@Service
public class CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	// adds a company into the database given the company name
	// the ID of the company is auto incremented in the database
	public Company addCompany(Company company) {
		return companyRepository.save(company);
	}
	
	// returns the list of all companies
	public Iterable<Company> getAllCompanies() {
		return companyRepository.findAll();
	}
	
	// returns the list of all companies
	public Company getCompanyById(int id) {
		return companyRepository.findById(id)
				.orElseThrow(()-> new CompanyNotFoundException(id));
	}
}
