package com.chatbot.api.services;

public class NoDefaultResourceException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NoDefaultResourceException() {
		super("Could not find a default resource ");
	}
}
