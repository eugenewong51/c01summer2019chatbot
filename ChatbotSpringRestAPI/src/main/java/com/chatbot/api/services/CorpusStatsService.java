package com.chatbot.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.api.controllers.StatNotFoundException;
import com.chatbot.api.models.CorpusStat;
import com.chatbot.api.repository.CorpusStatsRepository;

@Service
public class CorpusStatsService {

  @Autowired
  CorpusStatsRepository repo;

  public CorpusStat findByQuestionId(String questionId) {
    return repo.findByQuestionId(questionId);
  }

  public CorpusStat addCorpusStat(CorpusStat corpusStat) {
    return repo.save(corpusStat);
  }

  //returns the list of all corpus stats
  public Iterable<CorpusStat> getAllStats() {
    return repo.findAll();
  }

  // increment top doc count for corpus stat
  public CorpusStat incrementTopDocCount(String questionId) {
    repo.updateTopDocCount(questionId);
    return repo.findByQuestionId(questionId);
  }

  // increment relevant doc count for corpus stat
  public CorpusStat incrementRelevantDocCount(String questionId) {
    repo.updateRelevantDocCount(questionId);
    return  this.findByQuestionId(questionId);
  }

  /*
   * public CorpusStat incrementRelevantDocCount(CorpusStat cs){ return
   * cs.incrementRelevantDocCount(); }
   */

  // update avg confidence score 
  public CorpusStat updateAvgConfidence(String questionId, double confidence) {
    CorpusStat stat = repo.findByQuestionId(questionId);
    if (stat == null) {
      throw(new StatNotFoundException(1));
    }
    double old_avg_confidence = stat.getAvgConfidence();
    int total_relevant_search_count = stat.getRelevantDocCount();
    double new_avg_confidence = (old_avg_confidence + confidence) / total_relevant_search_count; 
    repo.updateAvgConfidence(questionId, new_avg_confidence);
    return repo.findByQuestionId(questionId);
  }

  //update avg confidence score 
  public List<CorpusStat> getTopFiveQuestions() {
    return repo.getTopFiveQuestions();
  }

  //get all stats sort by column
  public List<CorpusStat> getAllStatsSortBy(String column, int size) {
    if(column.equalsIgnoreCase("RELEVANT_DOC_COUNT")) {
      return repo.getAllStatsSortByRelevantDocCount(size);
    } else if (column.equalsIgnoreCase("AVG_CONFIDENCE")) {
      return repo.getAllStatsSortByAvgConfidence(size);
    }else {
      return repo.getAllStatsSortByTopDocCount(size);
    }
  }

  //delete a statistic
  public void deleteStat(int id) {
    repo.deleteById(id);
  }

}
