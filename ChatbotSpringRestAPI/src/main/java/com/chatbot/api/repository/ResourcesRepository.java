package com.chatbot.api.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.chatbot.api.models.Resources;

@Repository
public interface ResourcesRepository extends CrudRepository<Resources, Integer>{

	@Query(value = "SELECT * FROM RESOURCES WHERE COMPANY_ID = :id", nativeQuery = true)
	public List<Resources> findByCompany(@Param("id") int companyId);
	
	
	@Query(value = "SELECT * FROM RESOURCES WHERE is_default = true", nativeQuery = true)
	public Optional<Resources> findByIsDefault();
	
	@Transactional
	@Modifying
	@Query(value = "update Resources r set r.isDefault = true where r.name IN (:name)")
	public void markAllDefault(String name);

	@Transactional
	@Modifying
	@Query(value = "update Resources r set isDefault = false")
	public void markAllNotDefault();

	@Transactional
	@Modifying
	@Query(value = "delete Resources r where r.name IN (:name)")
	public void deleteResourceGroup(String name);

	@Query(value = "SELECT * FROM RESOURCES WHERE is_default = true", nativeQuery = true)
	public List<Resources> findAllByIsDefault();

}
