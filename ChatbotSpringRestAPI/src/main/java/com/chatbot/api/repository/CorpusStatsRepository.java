package com.chatbot.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chatbot.api.models.CorpusStat;

@Repository
public interface CorpusStatsRepository extends CrudRepository<CorpusStat, Integer> {

      public CorpusStat findByQuestionId(String question_id);
      
      @Query(value = "SELECT * FROM CORPUS_STATS ORDER BY TOP_DOC_COUNT DESC LIMIT :size ", nativeQuery = true)
      public List<CorpusStat> getAllStatsSortByTopDocCount(@Param("size") int size);
      
      @Query(value = "SELECT * FROM CORPUS_STATS ORDER BY RELEVANT_DOC_COUNT DESC LIMIT :size ", nativeQuery = true)
      public List<CorpusStat> getAllStatsSortByRelevantDocCount(@Param("size") int size);
      
      @Query(value = "SELECT * FROM CORPUS_STATS ORDER BY AVG_CONFIDENCE DESC LIMIT :size ", nativeQuery = true)
      public List<CorpusStat> getAllStatsSortByAvgConfidence(@Param("size") int size);
      
      @Transactional
      @Modifying(flushAutomatically = true, clearAutomatically = true)
      @Query(value = "UPDATE CORPUS_STATS SET TOP_DOC_COUNT = TOP_DOC_COUNT + 1 WHERE QUESTION_ID = :id", nativeQuery = true)
      public void updateTopDocCount(@Param("id") String question_id);
      
      @Transactional
      @Modifying(flushAutomatically = true, clearAutomatically = true)
      @Query(value = "UPDATE CORPUS_STATS SET RELEVANT_DOC_COUNT = RELEVANT_DOC_COUNT + 1 WHERE QUESTION_ID = :id", nativeQuery = true)
      public int updateRelevantDocCount(@Param("id") String question_id);
      
      @Transactional
      @Modifying(flushAutomatically = true, clearAutomatically = true)
      @Query(value = "UPDATE CORPUS_STATS SET AVG_CONFIDENCE = :avg WHERE QUESTION_ID = :id", nativeQuery = true)
      public void updateAvgConfidence(@Param("id") String question_id, @Param("avg") double avg_confidence);

      @Query(value = "SELECT * FROM CORPUS_STATS ORDER BY TOP_DOC_COUNT DESC LIMIT 5 ", nativeQuery = true)
      public List<CorpusStat> getTopFiveQuestions();
     
}
