package com.chatbot.api.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.chatbot.api.models.Stats;

@Repository
public interface StatsRepository extends CrudRepository<Stats, Integer>{

	public List<Stats> findByUserID(int user);
	
	public Stats findBySessionId(String sessionId);
	
	@Transactional
	@Modifying(flushAutomatically = true, clearAutomatically = true)
	@Query(value = "UPDATE STATS SET MESSAGE_COUNT = MESSAGE_COUNT + 1 WHERE SESSION_ID = :id", nativeQuery = true)
	public void updateMessageCount(@Param("id") String id);
	
	@Transactional
	@Modifying(flushAutomatically = true, clearAutomatically = true)
	@Query(value = "UPDATE STATS SET NO_RESPONSE_COUNT = NO_RESPONSE_COUNT + 1 WHERE SESSION_ID = :id", nativeQuery = true)
	public void updateNoResponseCount(@Param("id") String id);
	
	@Query(value = "SELECT * FROM STATS WHERE  DATE >= :startDate AND DATE <= :endDate", nativeQuery = true)
	public List<Stats> findByDateRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
