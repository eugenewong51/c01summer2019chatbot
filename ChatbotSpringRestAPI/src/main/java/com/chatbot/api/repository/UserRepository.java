package com.chatbot.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chatbot.api.models.BotUser;
@Repository
public interface UserRepository extends CrudRepository<BotUser, Integer>{
	
	public BotUser findByGoogleId(String googleId);
}
