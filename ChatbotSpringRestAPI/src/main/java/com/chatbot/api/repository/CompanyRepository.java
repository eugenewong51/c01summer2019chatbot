package com.chatbot.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chatbot.api.models.Company;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Integer>{

}
