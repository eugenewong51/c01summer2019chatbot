package com.chatbot.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.api.models.CorpusStat;
import com.chatbot.api.services.CorpusStatsService;

@CrossOrigin
@RestController
@RequestMapping("/corpus-stats")
public class CorpusStatsController {

  @Autowired
  private CorpusStatsService service;

  // returns the list of all corpus stats
  @GetMapping(produces = "application/json")
  public @ResponseBody ResponseEntity<Iterable<CorpusStat>> getAllStats() {
    return ResponseEntity.ok(service.getAllStats());
  }

  //returns the selected statistic by user Id
  @GetMapping(path = "/sort-by/{column}/{size}", produces = "application/json")
  public @ResponseBody ResponseEntity<List<CorpusStat>> getAllStatsSortBy(@PathVariable(required = true) String column, @PathVariable(required = true) int size) {
    return ResponseEntity.ok(service.getAllStatsSortBy(column, size));
  }
  
  //delete a corpus stas
  @DeleteMapping(path = "delete/{id}")
  public void deleteStat(@PathVariable(required = true) int id) {
      service.deleteStat(id);
  }

}
