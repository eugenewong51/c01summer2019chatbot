package com.chatbot.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.api.models.Company;
import com.chatbot.api.services.CompanyService;

@CrossOrigin
@RestController
public class CompanyController {

	@Autowired
	private CompanyService service;

	// adds a company into the database given the company name
	// the ID of the company is auto incremented in the database
	@PostMapping(path = "/company", consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseEntity<Company> addCompany(@RequestBody Company company) {
		service.addCompany(company);
		return ResponseEntity.ok(company);
	}
	
	// returns the list of all companies
	@GetMapping(path = "/company", produces = "application/json")
	public @ResponseBody ResponseEntity<Iterable<Company>> getAllCompanies() {
		return ResponseEntity.ok(service.getAllCompanies());
	}
	
	// returns the list of all companies
	@GetMapping(path = "/company/{id}", produces = "application/json")
	public @ResponseBody ResponseEntity<Company> getCompanyById(@PathVariable(required = true) int id) {
		return ResponseEntity.ok(service.getCompanyById(id));
	}
}
