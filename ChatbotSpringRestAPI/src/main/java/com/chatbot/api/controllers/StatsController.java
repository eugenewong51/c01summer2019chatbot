package com.chatbot.api.controllers;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.api.models.Stats;
import com.chatbot.api.services.StatsService;

@CrossOrigin
@RestController
@RequestMapping("/stats")
public class StatsController {

	@Autowired
	private StatsService service;
	
	// adds a statistic into the database given the userId, date, messageCount, and noResponseCount
	// the ID of the resource is auto incremented in the database
	@PostMapping(consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseEntity<Stats> addStat(@RequestBody Stats stats) {
		service.addStat(stats);
		return ResponseEntity.ok(stats);
	}
	
	// returns the list of all stats
	@GetMapping(produces = "application/json")
	public @ResponseBody ResponseEntity<Iterable<Stats>> getAllStats() {
		return ResponseEntity.ok(service.getAllStats());
	}
	
	// return a statistic based on SessionId
	@GetMapping(path = "/sessionId/{sessionId}", produces = "application/json")
	public @ResponseBody ResponseEntity<Stats> getStatsBySessionId(@PathVariable String sessionId) {
		return ResponseEntity.ok(service.getStatsBySessionId(sessionId)); 
	}
	
	// returns the selected statistic
	@GetMapping(path = "/{id}", produces = "application/json")
	public @ResponseBody ResponseEntity<Stats> getStatsById(@PathVariable(required = true) int id) {
		return ResponseEntity.ok(service.getStatsById(id));
	}

	// returns the selected statistic by user Id
	@GetMapping(path = "/userId/{user}", produces = "application/json")
	public @ResponseBody ResponseEntity<List<Stats>> getStatsByUserId(@PathVariable int user) {
		return ResponseEntity.ok(service.getStatsByUserId(user));
	}

	// delete a statistic
	@DeleteMapping(path = "/{id}")
	public void deleteStat(@PathVariable(required = true) int id) {
		service.deleteStat(id);;
	}
	
	@PutMapping(path = "/updateNoResponse/{sessionId}", produces = "application/json")
	public @ResponseBody ResponseEntity<Stats> updateNoResponseCount(@PathVariable String sessionId) {
		return ResponseEntity.ok(service.updateNoResponseCount(sessionId));
	}
	
	@PutMapping(path = "/updateMessageCount/{sessionId}", produces = "application/json")
	public @ResponseBody ResponseEntity<Stats> updateMessageCount(@PathVariable String sessionId) {
		return ResponseEntity.ok(service.updateMessageCount(sessionId));
	}
	
	@GetMapping(path = "/date", consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseEntity<List<Stats>> getStatsByDateRange(@RequestBody List<Date> range) {
		return ResponseEntity.ok(service.getStatsByDateRange(range.get(0), range.get(1)));
	}
}
