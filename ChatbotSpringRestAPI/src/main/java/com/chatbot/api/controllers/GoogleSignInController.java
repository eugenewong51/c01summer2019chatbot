package com.chatbot.api.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;

import com.chatbot.api.beans.GoogleUserIdToken;
import com.chatbot.api.beans.TokenVerification;
import com.chatbot.api.models.BotUser;
import com.chatbot.api.repository.UserRepository;

@RestController
@CrossOrigin
@SessionAttributes("UserBot")
public class GoogleSignInController {
	// temporarily storing client id here
	// in future this could be saved globally
	// or managed by a WebSecurityConfigurer
	@Autowired
	private UserRepository userRepository;
	private String clientId = "1006176182232-8jmor4umefj86vd7p4j52c4hbk98mdeb.apps.googleusercontent.com";
	TokenVerification tVerification;
	
	GoogleSignInController(){
		tVerification = new TokenVerification();
	}
	
	/*mapping to verify user id token from, front end
	 *google login
	 */
	@PostMapping(path="/tokensignin",consumes = "application/json", produces = "application/json")
	TokenVerification verify(@RequestBody GoogleUserIdToken token, HttpServletRequest request) throws GeneralSecurityException, IOException {
		
		//get a http transport
		HttpTransport transport = new NetHttpTransport();
		// get jsonfactory
		JsonFactory jsonFactory = new JacksonFactory();
		// build a token verifyer
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
			    // Specify the CLIENT_ID of the app that accesses the backend:
			    .setAudience(Collections.singletonList(clientId))
			    .build();
		
//		System.out.println("verifying token...");
		GoogleIdToken idToken = verifier.verify(token.getToken());
		if (idToken != null) {
			  Payload payload = idToken.getPayload();
			  System.out.println("getting user...");
			  // Print user identifier
			  String googleId = payload.getSubject();
			  System.out.println("User ID: " + googleId);

			  // Get profile information from payload
			  String email = payload.getEmail();
			  boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
			  String name = (String) payload.get("name");
			  String pictureUrl = (String) payload.get("picture");
			  String locale = (String) payload.get("locale");
			  String familyName = (String) payload.get("family_name");
			  String givenName = (String) payload.get("given_name");

			  // Use or store profile information
			  //check if the user exists
//			  System.out.println("saving user...");
			  if(null == userRepository.findByGoogleId(googleId)) {
				  BotUser user = new BotUser();
				  
				  user.setGoogleId(googleId);
				  user.setGivenName(givenName);
				  user.setEmail(email);
				  user.setPictureUrl(pictureUrl);
				  user.setFamilyName(familyName);
				  
				  userRepository.save(user);
			  }
			//find user by google id
			BotUser user = this.userRepository.findByGoogleId(googleId);
		    tVerification.setUserId(user.getID());
		    tVerification.setValid(true); 
			
		    // add the user in the session
		    request.getSession().setAttribute("userid", user.getID());
		    request.getSession().setAttribute("username", user.getGivenName());
		    
		    
			} else {
			  System.out.println("Invalid ID token.");
			  tVerification.setUserId(-1); //-1 for invalid user for now 
			  tVerification.setValid(false);
			}
		
		return tVerification;
		
	}
	
}
