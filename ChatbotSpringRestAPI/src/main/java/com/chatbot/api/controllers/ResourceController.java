package com.chatbot.api.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.api.models.Company;
import com.chatbot.api.models.Resources;
import com.chatbot.api.repository.CompanyRepository;
import com.chatbot.api.repository.ResourcesRepository;
import com.chatbot.api.services.ResourcesService;
import com.chatbot.core.ConnectionFailedException;
import com.chatbot.core.IndexExistsException;
import com.chatbot.core.LuceneDriver;
import com.chatbot.core.NoSuchIndexException;

@CrossOrigin
@RestController
@RequestMapping("/resources")
public class ResourceController {

	@Autowired
	private ResourcesService service;
	
	// adds a resource into the database given the resource location and company
	// the ID of the resource is auto incremented in the database
	@PostMapping(consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseEntity<Iterable<Resources>> addResource(@RequestBody Resources resource) {
		System.out.println(resource);
		Iterable<Resources> result = null;
		try {
			result = service.addResourceOnCrawlRequest(resource);
		} catch (ConnectionFailedException e) {
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
								.body(result);
		} catch (IOException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
			              .body(result);
		} catch (IndexExistsException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
			               .body(result);
		}
		return ResponseEntity.ok(result);
	}


	// adds a resource into the database given the resource location and company
	// the ID of the resource is auto incremented in the database
	@PutMapping(consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseEntity<Iterable<Resources>> updateResource(@RequestBody Resources resource) {
		// System.out.println(resource);
		Iterable<Resources> result = null;
		try {
			result = service.updateResourceOnCrawlRequest(resource);
		} catch (ConnectionFailedException e) {
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
								.body(result);
		} catch (IOException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
			              .body(result);
		} catch (NoSuchIndexException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
			               .body(result);
		}
		return ResponseEntity.ok(result);
	}

	@PostMapping(path = "/{name}/markDefault", produces = "application/json")
	public @ResponseBody void markDefault(@PathVariable(required = true) String name) {
		service.markAllDefault(name);
	}

	
	// returns the list of all resources
	@GetMapping(produces = "application/json")
	public @ResponseBody ResponseEntity<Iterable<Resources>> getAllResources() {
		return ResponseEntity.ok(service.getAllResources());
	}
	
	// returns the a selected resource
	@GetMapping(path = "/{id}", produces = "application/json")
	public @ResponseBody ResponseEntity<Resources> getResourcesById(@PathVariable(required = true) int id) {
		return ResponseEntity.ok(service.getResourcesById(id));
	}

	// returns the resources based on a company ID
	@GetMapping(path = "/byCompany/{companyId}", produces = "application/json")
	public @ResponseBody ResponseEntity<List<Resources>> getResourcesByCompany(@PathVariable int companyId) {
		return ResponseEntity.ok(service.getResourcesByCompany(companyId));
	}
	
	// delete a resource
	@DeleteMapping(path="/{id}/deleteOne", produces = "application/json")
	public void deleteResource(@PathVariable(required = true) int id)
			throws IOException, NoSuchIndexException {
		service.deleteResource(id);
	}

	// delete a resource
	@DeleteMapping(path="/{name}",produces = "application/json")
	public void deleteResourceGroup(@PathVariable(required = true) String name)
			throws IOException, NoSuchIndexException {
		service.deleteResourceGroup(name);
	}
}
