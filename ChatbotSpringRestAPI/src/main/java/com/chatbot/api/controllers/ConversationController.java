package com.chatbot.api.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.chatbot.api.beans.Query;
import com.chatbot.api.models.BotUser;
import com.chatbot.api.models.CorpusStat;
import com.chatbot.api.repository.UserRepository;
import com.chatbot.api.services.CorpusStatsService;
import com.chatbot.core.LuceneDriver;
import com.chatbot.core.LuceneSearcher;
import com.chatbot.core.OpenNlpAnalyzer;
import com.chatbot.core.watson.Actions;
import com.chatbot.core.watson.WatsonManager;
import com.chatbot.core.watson.discovery.Connecter;
import com.ibm.watson.assistant.v2.model.DialogNodeAction;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.discovery.v1.model.QueryResult;

@CrossOrigin
@RestController
public class ConversationController {

  // instantiate IBM discovery helper
  Connecter discovery = Connecter.getInstance();

  // Create WatsonManager
  WatsonManager watsonManager = WatsonManager.get_Watson_Manager();

  //
  OpenNlpAnalyzer analyzer = new OpenNlpAnalyzer();
  LuceneSearcher luceneSearcher = new LuceneSearcher(analyzer);

  private UserRepository userRepository;
  
  @Autowired
  private CorpusStatsService corpusService;

  /*
   * response to a query allow request from any origin for now
   */
  @SuppressWarnings("unchecked")
  @CrossOrigin
  @PostMapping(path = "/user-query", consumes = "application/json", produces = "application/json")
  MessageResponse reply(@RequestBody Query query, HttpServletRequest request) {

    // print the user in the session
    // System.out.println(request.getSession().getAttribute("userid"));
    // System.out.println(request.getSession().getAttribute("username"));

    // Pass query to Watson Assistant
    String session_id = request.getSession().getId();
    MessageResponse response = watsonManager.send_message(session_id, query.getQuery());

    // Check for any actions requested by the assistant
    String action = "";
    List<DialogNodeAction> responseActions = response.getOutput().getActions();
    if (responseActions != null) {
      if (responseActions.get(0).getActionType().equals("client")) {
        action = responseActions.get(0).getName();
      }
    }

    String userid = "";
    String role = "";
    Optional<BotUser> user;
    if (request.getSession().getAttribute("userid") != null) {
      userid = request.getSession().getAttribute("userid").toString();
      user = userRepository.findById(Integer.valueOf(userid));
      if (user != null) {
        role = user.get().getUserRole();
      } else {
        role = Roles.ANONYMOUS.toString();
      }
    } else {
      role = Roles.ANONYMOUS.toString();
    }

    // System.out.println(action);
    // System.out.println(role);
    // System.out.println(action);

    boolean adminTask = ((action.equals(Actions.VIEW_DASHBOARD.toString()))
        || (action.equals(Actions.UPDATE_INDEX.toString()))
        || (action.equals(Actions.UPDATE_DISCOVERY.toString()))) && role.equals("admin");

    if (action.equals(Actions.DISPLAY_RESPONSE.toString())) {
      response.getOutput().getUserDefined().put("type", Actions.DISPLAY_RESPONSE.toString());
      return response;
    } else if (adminTask) {
      response.getOutput().getUserDefined().put("type", action);
      return response;
    } else if (action.equals(Actions.GET_TOP_QUESTIONS.toString())) {
      prepareTopQuestions(response);
    } else {
      prepareDiscoveryLuceneResponse(response, query);
    }
    
    return response;
  }

  @SuppressWarnings("unchecked")
  private void prepareDiscoveryLuceneResponse(MessageResponse response, Query query) {
    List<QueryResult> res;
    res = discovery.askDiscovery(query.getQuery()).getResults();

    if (!res.isEmpty()) {

      CorpusStat cStat = corpusService.findByQuestionId(res.get(0).getId());
      if (cStat == null) {
        cStat = new CorpusStat();
        cStat.setQuestionID(res.get(0).getId());
        cStat.setQuestion(res.get(0).get("question").toString()); // it's a list of questions not one question
        cStat.setRelevantDocCount(0);
        cStat.setTopDocCount(0);
        cStat.setAvgConfidence(0);  
      }
      cStat.incrementTopDocCount();
      corpusService.addCorpusStat(cStat);
      
      for (QueryResult qres : res) {
        cStat = corpusService.findByQuestionId(qres.getId());
        if (cStat == null) {
          cStat = new CorpusStat();
          cStat.setQuestionID(qres.getId());
          cStat.setQuestion(qres.get("question").toString()); // it's a list of questions not one question
          cStat.setRelevantDocCount(0);
          cStat.setTopDocCount(0);
          cStat.setAvgConfidence(0);
        }
        
        cStat.setQuestion(qres.get("question").toString());
        cStat.incrementRelevantDocCount();
        cStat.updateAverageConfidence(qres.getResultMetadata().getConfidence());

        //System.out.println(qres.getId());
        //System.out.println("\t" + qres.get("question"));
        //System.out.println("\t\t" + qres.get("answer"));
        //System.out.println("\t\t\t" + qres.getResultMetadata().getConfidence());
        //System.out.println(response.getOutput().getActions());
        
        corpusService.addCorpusStat(cStat);
      }          

      if (res.get(0).getResultMetadata().getConfidence() >= 0.15) {
        response.getOutput().getUserDefined().put("type", Actions.DISPLAY_DISCOVERY.toString());
        response.getOutput().getUserDefined().put("discovery", res); 
        return;
      } 

    } 

    String luceneResponse = ""; 
    try { 
      luceneResponse = LuceneDriver.search(query.getQuery()); 
      System.out.println(luceneResponse);
      if (luceneResponse.equals("[]")) {
        response.getOutput().getUserDefined().put("type", Actions.NO_RESPONSE.toString());
      } else {
        response.getOutput().getUserDefined().put("type", Actions.DISPLAY_LUCENE.toString());
        response.getOutput().getUserDefined().put("lucene", luceneResponse);
      }
    } catch (IOException e) { //
      e.printStackTrace(); 
      response.getOutput().getUserDefined().put("type", Actions.NO_RESPONSE.toString());
    }

    return;
  }

  @SuppressWarnings("unchecked")
  private void prepareTopQuestions(MessageResponse response) {
    List<CorpusStat> topCorpusStats = corpusService.getTopFiveQuestions();

    List<String> topQuestions = new ArrayList<String>();

    for (CorpusStat corpusStat : topCorpusStats) {
      topQuestions.add(corpusStat.getQuestion());
    }

    response.getOutput().getUserDefined().put("type", Actions.GET_TOP_QUESTIONS.toString());
    response.getOutput().getUserDefined().put("questions", topQuestions);
  }

}
