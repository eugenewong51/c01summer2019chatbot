package com.chatbot.api.controllers;

public class DefaultResourceNotFound extends RuntimeException {

	private static final long serialVersionUID = 6845555588949706318L;

    public DefaultResourceNotFound() {
		super("No resource is marked default");
	}
}
