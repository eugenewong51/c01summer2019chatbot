package com.chatbot.api.controllers;

public class ResourceNotFoundException extends RuntimeException {

	public ResourceNotFoundException(int id) {
		super("Could not find resource with id: " + id);
	}
}
