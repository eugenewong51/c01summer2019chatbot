package com.chatbot.api.controllers;

public class UserNotFoundException extends RuntimeException {
	
	public UserNotFoundException(int id){
		super("Could not find the user " + id);
	}
}
