package com.chatbot.api.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.chatbot.api.models.CorpusStat;
import com.chatbot.api.services.CorpusStatsService;
import com.chatbot.core.watson.discovery.DocumentController;
import com.ibm.watson.discovery.v1.model.DeleteDocumentResponse;
import com.ibm.watson.discovery.v1.model.DocumentAccepted;
import com.ibm.watson.discovery.v1.model.DocumentStatus;
import com.ibm.watson.discovery.v1.model.QueryResponse;

@CrossOrigin
@RestController
@RequestMapping("/dfi/discovery")
public class DFICorpusController {
	private DocumentController dc = new DocumentController();
	
	@Autowired
	private CorpusStatsService service;
	
	@GetMapping("/document")
	public @ResponseBody QueryResponse getAllDocs() {
		return dc.getAllDocuments();
	}
	
	@PostMapping("/document")
	public @ResponseBody DocumentAccepted addDocument(@RequestBody(required=true) String body) {
			
			DocumentAccepted da = dc.addDocument(body);
			CorpusStat cStat = new CorpusStat();
			cStat.setQuestionID(da.getDocumentId());
			cStat.setTopDocCount(0);
			cStat.setRelevantDocCount(0);
			cStat.setAvgConfidence(0);
			service.addCorpusStat(cStat);
			
			return da;
	}

	@PostMapping("/document/{documentId}")
	public @ResponseBody DeleteDocumentResponse removeDocument(@PathVariable(required=true) String documentId) {
		return dc.deleteDocument(documentId);

	}
	
	@PutMapping("/document/{documentId}")
	public @ResponseBody DocumentAccepted editDocument(@RequestBody(required=true) String body,@PathVariable(required=true) String documentId) {
		System.out.println(body);
		return dc.editDocument(body,documentId);
	}
	
	@GetMapping("/document/{documentId}/info")
	public @ResponseBody DocumentStatus getDocInfo(@PathVariable(required=true) String documentId) {
		return dc.getDocumentInfo(documentId);
	}
	
	@GetMapping("/document/{documentId}")
	public @ResponseBody QueryResponse getDocByID(@PathVariable(required=true) String documentId) {
		return dc.getDocumentById(documentId);
	}
	
	@PostMapping("/document/upload")
	public @ResponseBody DocumentAccepted uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
		System.out.println(file);
		return dc.addDocx(file.getInputStream());
	}
	
	@DeleteMapping("/document/{documentId}")
	public @ResponseBody DeleteDocumentResponse deleteDocumentById(@PathVariable(required=true) String documentId) {
		return dc.deleteDocument(documentId);
	}
	
}
