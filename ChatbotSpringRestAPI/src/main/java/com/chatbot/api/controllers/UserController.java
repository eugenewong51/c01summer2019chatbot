package com.chatbot.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.api.models.BotUser;
import com.chatbot.api.services.UserService;

@CrossOrigin
@RestController
public class UserController {
	
	@Autowired
	private UserService service;
	
	
	@GetMapping(path = "/users", produces = "application/json")
	List<BotUser> all(){
		return service.getAllUsers();
	}
	
	@PostMapping(path = "/users", consumes = "application/json", produces = "application/json")
	BotUser newBotUser(@RequestBody BotUser newBotUser) {
		service.addUser(newBotUser);
		return newBotUser;
	}
	
	@GetMapping("/users/{id}")
	BotUser one(@PathVariable int id) {
		return service.getUserById(id);
	}
	
	
	@DeleteMapping("/users/{id}")
	void deleteBotUser(@PathVariable int id) {
		service.deleteUser(id);
	}
}
