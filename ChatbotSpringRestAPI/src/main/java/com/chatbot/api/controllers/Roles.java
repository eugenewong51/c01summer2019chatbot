package com.chatbot.api.controllers;

public enum Roles {
  ADMIN,
  USER,
  ANONYMOUS
}
