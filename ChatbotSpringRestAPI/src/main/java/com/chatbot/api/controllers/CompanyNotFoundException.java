package com.chatbot.api.controllers;

public class CompanyNotFoundException extends RuntimeException {

	public CompanyNotFoundException(int id) {
		super("Could not find company with ID: " + id);
	}
}
