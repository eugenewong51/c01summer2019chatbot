package com.chatbot.api.controllers;

public class StatNotFoundException extends RuntimeException {

	public StatNotFoundException(int id) {
		super("Could not find statistic with id: " + id);
	}
}
