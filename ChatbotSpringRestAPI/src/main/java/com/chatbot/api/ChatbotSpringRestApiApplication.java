package com.chatbot.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.time.LocalDate;
import java.util.Date;
import java.util.Random;

import com.chatbot.api.models.BotUser;
import com.chatbot.api.models.Company;
import com.chatbot.api.models.Resources;
import com.chatbot.api.models.Stats;
import com.chatbot.api.services.CompanyService;
import com.chatbot.api.services.ResourcesService;
import com.chatbot.api.services.StatsService;
import com.chatbot.api.services.UserService;
import com.ibm.cloud.objectstorage.auth.policy.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

@EnableConfigurationProperties
@EnableAutoConfiguration
//@EnableJdbcHttpSession
@SpringBootApplication
public class ChatbotSpringRestApiApplication implements  ApplicationRunner{
	
	@Autowired
	private CompanyService companyService;

	@Autowired
	private ResourcesService resourcesService;
	
	@Autowired
	private StatsService statsService;

	@Autowired
	private UserService userService;
    private ServerProperties serverProperties;

	public static void main(String[] args) {
		SpringApplication.run(ChatbotSpringRestApiApplication.class, args);
	}
	

	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println(serverProperties);
		
		System.out.println("seeding necessary data...");

		// save one company for testing by deafault
		Company company  = new Company();
		company.setCompanyName("DFI");
		companyService.addCompany(company);
		


		// for (int i = 1; i < 6; i++) {
		// 	BotUser user = new BotUser();
		// 	user.setCompanyID("DFI");
		// 	user.setEmail("testEmail" + i);
		// 	user.setFamilyName("testFam" + i);
		// 	user.setGivenName("testGiv" + i);
		// 	user.setPictureUrl("testPic" + i);
		// 	user.setToken("testToken" + i);
		// 	user.setUserRole("anonymous");
		// 	user.setGoogleId("testGoogleId" + i);
			
		// 	userService.addUser(user);
		// }
		
		// for (int i = 1; i < 17; i++) {
		// 	Stats stat = new Stats();
		// 	Random rand = new Random();
		// 	stat.setMessageCount(rand.nextInt(20) + 10);
		// 	stat.setNoResponseCount(rand.nextInt(5));
		// 	stat.setSessionId("testSession" + i);
		// 	stat.setUser(userService.getUserById(rand.nextInt(5) + 1));
			
		// 	statsService.addStat(stat);
		// }
		// Resources resource  =new Resources();
		// resource.setCompany(company);
		// resource.setName(Integer.toString(new Random().nextInt(2345234)));
		// resource.setOrigin("http://utsc.utoronto.ca");
		// resource.setDefault(true);
		// resource.setDepth(0);
		// Iterable<Resources> r = resourcesService.addResourceOnCrawlRequest(resource);
		// System.out.println("resources saves");

	}
}
