# **Chatbot Spring API**
This project serve RESTful API end points for the chatbot application frontend

# Live development version deployed
[Chatbot Spring API](https://c01-chatbot-api.herokuapp.com/) - Documentation included 

# Project Setup

### Compile the project
`mvn clean package`

### Run a local developemnt server
`java -jar target/ChatbotSpringRestApi-dev.jar`