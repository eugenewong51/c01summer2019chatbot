
# Competition ###

### Where do we stand? ###
Our chatbot is similar to existing chatbots like those used by Facebook Messenger and Skype in the sense that our chatbot interfaces with clients in the form of a simple conversation inside a typical messaging application. However, unlike chat bots used by Facebook Messenger and Skype, our application will implement a RESTful API so that it can easily be integrated with any existing website as a web application. 

Furthermore, while many chatbots involve support personnel on the other end and require back-end integration with the existing key functionalities of a business, like modifying account information or making reservations our chatbot only requires front-end integration. Since clients can load text data in the form of text files or website links directly from the web interface and there is no need for back-end integration with the client our chatbot is the ideal choice for quick and seamless integration and ease of use. The additional ability to parse pdf, word and other document formats make the chatbot versatile so that any admin can feed the data they want and have it readily available. 

###  Who should be interested? and why?###
Our chat bot solution is especially suited for small businesses trying to make their company's public data more accessible and easier to navigate for potential customers. Even individuals creating data-oriented sites would find our chatbot a clean solution for presenting specific and relevant information to their customers. While previously mentioned chatbots are packaged with several advanced features and offer back-end integration with business services, these features may be excessive for the average use case where clients simply want to interface their data cleanly and elegantly with their customers. 

While other chatbots cater to big clients who may need extensive integration our chatbot provides a lightweight, easy to use, and simple integration alternative for those looking for a chatbot that just simply provides the right answers.  

### What are competitors doing? ###
A lot of competitors are investing in providing solutions with easy to use chat interfaces with natural feeling human to bot interaction. For example, IBM Watson focuses on building highly intelligent and powerful bots by using natural language processing combined with Artificial Intelligence. Although these bots can be highly sophisticated they are often a niche technology with most of its services hard to approach for smaller business and too complex for an average user to interact with. 
