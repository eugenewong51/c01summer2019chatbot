# User Stories

1) As a user, I want a chat bar to ask questions to the bot such that the bot can understand what I want answers for
	
    Difficulty: Medium
	Priority: Medium

2) As a user, I want my question to be read and the bot to send a link to a page which contains information relevant to my question such that I can find the answer to my question
    
    Difficulty: High
    Priority: Medium

3) As a user, I would like a registration page to register an account with the chat bot such that I can access special documents that have answers to my questions
    
    Difficulty: Medium
    Priority: Low

4) As an admin, I want to add a URL containing relevant information to my institution that will be stored by the bot for my clients
    
    Difficulty: High
    Priority: High
 
5) As an admin, I want to add a PDF and word files containing relevant information to my institution that will be stored by the bot for my clients
    
    Difficulty: Medium
    Priority: Low

6) As an admin, I want to add all resources including files and links containing relevant information to my institution such that only registered users can access these resources
    
    Difficulty: Medium
    Priority: Medium

7) As an admin,  I want to remove resources I have added previously so they become inaccessible from the chatbot
    
    Difficulty: Low
    Priority: Low

8) As a user, I would like a view of the chat history such that I can look at previous questions that have been answered and access the links provided by the bot
    
    Difficulty: High
    Priority: Low

9) As a user I want to be able to see other relevant results of my query if the bot's current response was not helpful
    
    Difficulty: Low
    Priority: Low

10) As a registered user, I want to be able to send feedback to admin when my query doesn't return any relevant results
    
    Difficulty: High
    Priority: Low