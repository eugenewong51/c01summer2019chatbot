

# Project Summary #

  

We list 4 key components of the ChatBot Project

  

### The Project Objective ###

  

Build an application where a user can learn about institutions and their resources by retrieving the necessary information while interacting with a conversational style Chat Bot. Optionally, users can also register for an account. Admins of these institutions have special privileges to add or remove relevant information associated with their institutions and decide which resources are limited to the registered users only.

  

### The Key Personas ###
#### More details in personas.md ####
  

* Andrea is an 18-year-old incoming first-year student at the University of Toronto Scarborough. She was an exceptional student in high school but was often described impatient by her teachers.

* Natalie is a 28-year-old DFI employee managing day to day inquiries from a broad range of clients and potential clients. She manages incoming client questions and works on developing strategies to improve their experience for new clients.

* David is a Financial Investor, managing 15 employees at Frontwater Capital and focuses mainly on clients investing in Fintech

  

### The Key Scenarios ###

  

* Andrea wants to be able to inform her parents when the fees are due or when important deadlines are approaching for a semester. She finds navigating through the UTSC website confusing, unfamiliar and tedious. She needs quick responses to the questions that will keep her updated.

  

* Natalie wants to provide more information about DFI and their resources to the users via chatbot to reduce thousands of email inquiries solely regarding basic common questions. He wants to be able to manage and control the availability of information efficiently. He would also like to know how frequently users use the chatbot to access DFI resources and on which topics.

  

* David has been experiencing an influx of clients asking for new updates in Fintech industry so they can move their investments. This happens frequently and on a daily basis. He wants to get his hand on the latest info DFI has any time he wants. He prefers to reach out to DFI on his specific needs while simultaneously trying to get answers of questions he has right now.

  

### The Key Principles ###

  

* More informed the students are, the better are their chances to do well in school- Students should be able to learn about a broad range of services on campus without having to email or confront a staff member. This encourages more students to stay informed and get the help they need.

  

* Time saved answering the same repetitive questions results in more time sent in helping clients that have more in-depth questions. This will drastically improve the experience of DFI employees in managing the clients that require more attention, by allocating more time to them, while simultaneously using the chatbot admin panel for helping the users that can get sufficient information from the chatbot.
