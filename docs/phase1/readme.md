# CSCC01 Chat Bot Project #

### What is this repository for? ###

* This repository is to maintain and develop code for CSCC01 Chat Bot Application
* Summary of the application can be found in summary.md

### How do I get set up? ###

* This will be updated once the project advances

### Contribution guidelines ###

* Merging a development branch into the master must be done via a pull request and must be approved by at least one reviewer
* All merges to the master must be fast-forward merge
* Our main contact channel is messenger
* Guidelines on code review to be updated 

### Team ###

* Dax Patel: dax.patel@utoronto.ca
* Vithursan Thiruvarooran: vithursan.thiruvarooran@utoronto.ca
* Jason Chen: jasonutsc.chen@mail.utoronto.ca
* Eugene Wong: eeugene.wong@mail.utoronto.ca

### Course Contacts ###
* Abbas Attarwala: abbas.attarwala@utoronto.ca
* Fredric Pun: frederic.pun@mail.utoronto.ca

