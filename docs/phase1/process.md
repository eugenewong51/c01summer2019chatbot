# Team Organization and Strategy #
### Vithursan, Jason, Dax and Eugene (left to right) ###

>![picture](https://bitbucket.org/mcs2/c01summer2019groupproject3/raw/2ab2e0777a3ddcf420a07c78d3ae9f2e4a67c072/docs/phase1/images/team.jpg)

### How did you organize the team? Which tools did you use, if any? ###

>The team is organized by having meetings on a weekly basis and discussing the work that will be completed within the next week. Some tools that we will use include JIRA for keeping track of the progress during sprints and setting up tasks throughout the development of the project.  We are using Facebook Messenger as a form of communication to ensure our team is on track with their work and for asking any questions when necessary. For prototyping, we are using Balsamiq with a student license to make wire-frames that we all can agree on as a group. 

### How did you make decisions? ###

> Decisions in our team are made after team discussion meetings where the decisions are only final when all team members come to an agreement and are clear on the contents of the discussions.

### How did you define priority and/or difficulty of user stories? ###

> The priority and difficulty of each user story were decided as a team through scrum poker. Each team member would decide on the difficulty and priority using a scale of low/med/high and we would try to convince each other why our ratings were better. The discussions went until all of us came to an agreed rating for each user story.

### Did you meet in person or online? ###

> Our team met both in person and online depending on the circumstances. Whenever possible, our team will meet in person to more clearly discuss issues and future plans. If one or more team members are not on campus or is unable to attend a team meeting, we resort to Facebook Messenger to continue conversations and discussions.

### How frequently did you meet? ###

> We meet in person at least once a week and are constantly discussing online to clarify any ideas or problems. 

### What lessons should you take forward to the next phase? ###

> We need to constantly keep communication with each other and to set a schedule for what we want to be completed within certain time frames. This will ensure our progress is not delayed and that we stay on track with our work for each sprint.
