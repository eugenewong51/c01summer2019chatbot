# 1) Andreea Gheorghe #
| Persona | First Year Student at UTSC | 
|--|--|
 | Photo |![picture](https://bitbucket.org/mcs2/c01summer2019groupproject3/raw/2ab2e0777a3ddcf420a07c78d3ae9f2e4a67c072/docs/phase1/images/andrea.png)  | 
 |Name|Andreea Gheorghe| 
 |Title| First Year Student at UTSC| 
 |Demographics| 18 years Old, Has GED, Oldest child with 1 year younger sister | 
 | Goals and tasks|She is excited to start school at UofT and has spent her summer preparing for it. She has combed over the UofT sites she could find and has tried gathering everything she can including deadlines, tuition fees, and course descriptions. Her duties include: Estimating her tuition in order to make financial plans, Research which courses she needs to take for her first semester what they are about, Find potential clubs and extracurricular activities related to her management field | 
 |Environment, obstacles and needs|She almost always works on her Macbook Pro usually at her desk or on her bed. She is not very organized and impatient which often leads her to forgetting things. Because she is impatient she often needs quick and immediate answers and hates having to search for very long.





# 2) Natalie Parker 
| Persona | Digital Finance Institute Associate |
|--|--|
| Photo |![picture](https://bitbucket.org/mcs2/c01summer2019groupproject3/raw/2ab2e0777a3ddcf420a07c78d3ae9f2e4a67c072/docs/phase1/images/natalie.png) |
|Name|Natalie Parker|
|Title| Financial Services Advisor, DFI |
|Demographics| 28 years Old, Single, Has M. Sc. and a B.B.A. from Schulich school of business|
| Goals and tasks|She is extremely motivated to inform consumers about their needs. For her, clients coming from startups and large corporations are equally important. Her duties include: Provide accurate and updated information to current clients, Manage high volumes of email inquiries that range from easy to difficult questions from potential clients, Guide new clients to get familiar with basic Fintech questions |
|Environment, obstacles and needs|She works on a computer with dual monitors. She spends most of her time on the desk to prioritize important client inquiries. Most of her work is done via email and often shuffles between the heavy volumes of emails from potential and current clients. She has never used a content management system of a website. She really needs to find a way to manage high demanding clients with trivial questions.|


# 3) David Lee #
| Persona  | Front water Capital, Manager |
|--|--|
| Photo  | ![picture](https://bitbucket.org/mcs2/c01summer2019groupproject3/raw/2ab2e0777a3ddcf420a07c78d3ae9f2e4a67c072/docs/phase1/images/david.png) |
|Name|David Lee|
|Title| Financial Investment Advisor, Front water Capital |
|Demographics| 43 years Old, Married with one child, Has master's degree in economics from UBC, Fintech enthusiast, Involved in ongoing research with UBC on effects of Fintech on financial industry |
| Goals and tasks|He manages a team of 15 employees at Front water Capital. He mainly focuses and directs investments in Fintech industry. His duties include: Providing his employees strategies for customers who need information on Fintech, Manage high volumes of customers asking about Fintech investments, Be able to find latest trends in Fintech industry |
|Environment, obstacles and needs|He usually works from his office and conducts brief updates on any Fintech related trends every other day. He doesn’t want to spend extra time finding documents for his clients through different sources. He has some very specific questions for DFI and wants to be able to ask it whenever possible. It's not ideal for him to wait for an email response from DFI. Some times the questions he gets are very repetitive so he likes to have a history of the list of questions he has directed to DFI so he can forward it to his employees.

