# Team Meetings

## Week 1: June 3 - 7

| Question | Expected | Actual |
| --- | --- | --- |
| Tell me about your team. More specifically who’s in your team and something about them | 5 | 5 |
| Tell me about DFI | 5 | 5 |
| Show me what you have so far | 5 | 5 |

**Comments**

 - Mentioned full name of DFI, talked about their audience, mentioned what they did
 - Was about to name and mention one thing about everyone in team
 - Competition was very throught

## Week 2: June 24 - 28

| Question | Expected | Actual |
| --- | --- | --- |
| Give me a summary of your application | 5 | 5 |
| Show me your progress with your screenshots and burndown chart | 5 | 5 |
| What’s the plan for the next sprint? | 5 | 5 |
| Show and walk through code, or talk about research and plans | 5 | 5 |

**Comments**

 - Covered main points of summary.md
 - Amazing burndown chart


## Week 3: June 24 - 28

| Question | Expected | Actual |
| --- | --- | --- |
| Show me your progress for week 3 | 5 | 5 |
| Show me your progress for week 4 | 5 | 5 |
| Tell me about the flow of your application | 5 | 5 |
| Run your application | 5 | 6 |

**Comments**

 - Bonus mark for amazing use of the time for feedback
 
## Week 4: July 22 - 26

|Question|Expected|Actual|
|--------|-----|----|
|Show me your progress for last week|5|5|
|Show me your progress for week before|5|5|
|Run me through your testing (or tell me your plans if not present)|5|5|
|Run the application (technical demo)|5|4|

**Comments**

- last sprint:
    - lucene
        - testing for lucene
        - finishing up search engine
    - database
    - testing
        - testing for springboot for controllers
        - debugging
    - watson
        - controller between watson and the frontend
    - frontend and watson discovery
        - integrating some of the work together
        - bringing discovery questions to the frontend
        - admin panel
- the week before:
    - mostly the same stuff
        - working towards the next sprint
        - researching, ...

- unit testing:
    - lots of unit testing exists
    - using mockito
    - most of the unit tests are passing; still working on full coverage

- demo:
    - ui looks pretty clean
        - corpus works
        - question responses are okay
    - web crawler isn't integrated with frontend
        - does work on the terminal
    - lucene file stuff is not fully integrated
    - login works alright
    - needed to see more backend integration for full marks

- jira:
    - burndown chart moves up and then goes down
        - added a lot of features from dfi why the burndown chart went down
        - finished a lot of story points but not all of them
    - sprint board
        - finished all the tasks in the sprint

## Week 5: July 29 - August 2

| Question | Expected | Actual |
| --- | --- | --- |
| Show me your progress for week 7 | 5 | 5 |
| Demo Application | 5 | 5 |

**Comments**

 - Burndown chart looks great, good progress to polish application
 - Very good UI, functionality, and is extremely innovative (Very well done)