# cscc01 -- phase 1

## administration:

- **marked by**: Kevin Zhang
- **email**: mstr.zhang@mail.utoronto.ca
- **remark policy**: consult email within 4 days of receiving mark

---

## mark breakdown

|section|mark|total|
|---|---|---|
|summary.md|25|25|
|competition.md|9|15|
|user_stories.md|23|25|
|process.md|25|25|
|personas.md|30|30|
|ui/ux|10|10|

**total mark:** 124/130 = 95%

---

## summary.md (25/25)

|criteria|mark|out of|comments|
|---|---|---|---|
|contains key users|5|5||
|contains scenarios|5|5||
|contains principles|5|5||
|contains objectives|5|5||
|presentation|5|5||

**additional comments**:

- N/A

---

## competition.md (9/15)

|criteria|mark|out of|comments|
|---|---|---|---|
|difference between products|1|5|some very false statements|
|relates back to users|3|5|does not relate to specific personas|
|presentation|5|5||

**additional comments**:

- does not relate back to specific personas defined earlier
- some very false statements
    - `However, unlike chat bots used by Facebook Messenger and Skype, our application will implement a RESTful API`
        - this is not true (Facebook Messenger also uses RESTful API)
        - this point is also not very relevant; why does this give you a competitive advantage to these other products?
    - `Since clients can load text data in the form of text files or website links directly from the web interface and there is no need for back-end integration with the client`
        - this statement is completely false; a web application needs to communicate with the backend as well
    - `our chatbot only requires front-end integration`
        - similar to above, this statement is completely false
- would suggest your team research the architecture of a RESTful application
    - sounds like you do not conceptually understand how a RESTful application works

---

## user_stories.md (23/25)

|criteria|mark|out of|comments|
|---|---|---|---|
|correct amount|5|5||
|has priority and difficulty|5|5||
|correct format|3|5|does not state specific personas|
|appropriateness|5|5||
|presentation|5|5||

**additional comments**:

- instead of stating "as an admin" or "as a user", state specific personas
    - e.g. "as Andrea", "as Natalie", "as David"

---

## process.md (25/25)

|criteria|mark|out of|comments|
|---|---|---|---|
|team photo|6|6||
|completeness|14|14||
|presentation|5|5||

**additional comments**:

- some extra points to think about (no marks deducted)
    - contingency planning
        - what if someone is not pulling their weight?
            - who picks up the slack?
            - what are the consequences?
    - who leads the discussions?
    - branching strategies
        - how do we ensure master remains safe?
        - what strategy are we going to use to push branches into master?

---

## personas (30/30)

|criteria|mark|out of|comments|
|---|---|---|---|
|correct amount|5|5||
|basic demographics|5|5||
|completeness|5|5||
|immediate goals|5|5||
|user goals|5|5||
|experience goals|5|5||

**additional comments**:

- N/A

---

## ui/ux (10/10)

|criteria|mark|out of|comments|
|---|---|---|---|
|completeness|10|10||

**additional comments**:

- N/A