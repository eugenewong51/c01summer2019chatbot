
# cscc01 project

**team number**: 3

---

## breakdown

### phase 1

|component|weight|out of|grade|
|---|---|---|---|
|phase 1|5%|130|124|

### phase 2

|component|weight|out of|grade|
|---|---|---|---|
|week 2 meeting|10%|20|20|
|week 3 meeting|10%|20|20|
|week 4 meeting|10%|20|19|
|week 5 meeting|10%|10|10|
|abbas meeting|20%|10|10|

### phase 3

|component|weight|out of|grade|
|---|---|---|---|
|video|5%|5|0|
|final demo|30%|100|100|

final demo caps at 100

---

## project grade

**final grade**: 94.26923077

final grade is calculated as `catme adjustment * grade`

|member|grade|catme adjustment|final grade|
|---|---|---|---|
|chenja17|94.26923077|1|94.26923077|
|pateldax|94.26923077|1|94.26923077|
|thiruva5|94.26923077|1|94.26923077|
|wongeu11|94.26923077|1|94.26923077|
